﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Services
{
    public class ReceiptService : IReceiptService
    {
        IReceipt repository;
        IContribution Repository_contribution;
        IOrganization Repository_organization;

        public ReceiptService(IReceipt repository, IContribution repository_contribution, IOrganization repository_organization)
        {
            this.repository = repository;
            Repository_contribution = repository_contribution;
            Repository_organization = repository_organization;
        }

        public Result Add(Receipt data)
        {
            var verifyData = data.isValid();

            if (verifyData.resultado) //verifico que los datos sean validos
            {
                //verifico que existe organization id
                if (this.Repository_contribution.FindById((int)data.ContributionId) == null)
                {
                    verifyData.listaErrores.Add(new Error(1, "Debe existir Contribution id"));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
                else if (this.Repository_organization.FindById((int)data.OrganizationId) == null)
                {
                    verifyData.listaErrores.Add(new Error(1, "Debe existir Organization id"));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
                else
                {
                    this.repository.Add(data);
                    return verifyData;
                }
            }
            else
            {
                //si los datos no son validos
                return verifyData;
            }
        }

        public Result Update(Receipt data)
        {
            var verifyData = data.isValid();

            if (verifyData.resultado) //verifico que los datos sean validos
            {
                //verifico que existe el registro 
                if (this.Repository_contribution.FindById((int)data.ContributionId) == null)
                {
                    verifyData.listaErrores.Add(new Error(1, "Debe existir Contribution id"));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
                else if (this.Repository_organization.FindById((int)data.OrganizationId) == null)
                {
                    verifyData.listaErrores.Add(new Error(1, "Debe existir Organization id"));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
                else if (this.repository.FindById((int)data.ReceiptId) == null)
                {
                    verifyData.listaErrores.Add(new Error(1, "No existe el registro que se quiere actualizar."));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
                else
                {
                    this.repository.Update(data);
                    return verifyData;
                }
            }
            else
            {
                //si los datos no son validos
                return verifyData;
            }
        }

        public Result Delete(Receipt data)
        {
            var verifyData = data.isValid();

            if (verifyData.resultado) //verifico que los datos sean validos
            {
                //verifico que existe el registro 
                if (this.repository.FindById((int)data.ReceiptId) != null)
                {
                    this.repository.Delete(data);
                    return verifyData;
                }
                else
                {
                    verifyData.listaErrores.Add(new Error(1, "No existe el registro que se quiere eliminar."));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
            }
            else
            {
                //si los datos no son validos
                return verifyData;
            }
        }

        public Result Delete(int id)
        {
            List<Error> errores = new List<Error>();
            var data = this.repository.FindById(id);
            //verifico que existe el registro 
            if (this.repository.FindById(id) != null)
            {
                this.repository.Delete(id);
                return new Result(true, null, data);
            }
            else
            {
                errores.Add(new Error(1, "No existe el id que se quiere eliminar"));
                return new Result(false, errores, null);
            }
        }

        public List<Receipt> FindAll()
        {
            return this.repository.FindAll();
        }

        public Receipt FindById(int id)
        {
            return this.repository.FindById(id);
        }
    }
}
