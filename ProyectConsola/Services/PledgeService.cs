﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Services
{
    public class PledgeService : IPledgeService
    {
        IPledge repository;
        static IOrganization Repository_organization;
        static IConstituent Repository_constituent;
        static IAppeal Repository_appeal;
        static ISolicitor Repository_solicitor;
        static ITribute Repository_tribute;

        public PledgeService(IPledge repository, IOrganization repository_organization, IConstituent repository_constituent, IAppeal repository_appeal, ISolicitor repository_solicitor, ITribute repository_tribute)
        {
            this.repository = repository;
            Repository_organization = repository_organization;
            Repository_constituent = repository_constituent;
            Repository_appeal = repository_appeal;
            Repository_solicitor = repository_solicitor;
            Repository_tribute = repository_tribute;
        }

        public Result Add(Pledge data)
        {
            var verifyData = data.isValid();

            if (verifyData.resultado) //verifico que los datos sean validos
            {
                //verifico que existan ids
                if (verifyIds(data) == null)
                {
                    this.repository.Add(data);
                    return verifyData;
                }
                else
                {
                    return verifyIds(data);
                }
            }
            else
            {
                //si los datos no son validos
                return verifyData;
            }
        }

        public Result Update(Pledge data)
        {
            var verifyData = data.isValid();

            if (verifyData.resultado) //verifico que los datos sean validos
            {
                //verifico que existan ids
                if (verifyIds(data) == null)
                {
                    this.repository.Add(data);
                    return verifyData;
                }
                else
                {
                    return verifyIds(data);
                }
            }
            else
            {
                //si los datos no son validos
                return verifyData;
            }
        }

        public Result Delete(Pledge data)
        {
            var verifyData = data.isValid();

            if (verifyData.resultado) //verifico que los datos sean validos
            {
                //verifico que existe el registro 
                if (this.repository.FindById((int)data.PledgeId) != null)
                {
                    this.repository.Delete(data);
                    return verifyData;
                }
                else
                {
                    verifyData.listaErrores.Add(new Error(1, "No existe el registro que se quiere eliminar."));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
            }
            else
            {
                //si los datos no son validos
                return verifyData;
            }
        }

        public Result Delete(int id)
        {
            List<Error> errores = new List<Error>();
            var data = this.repository.FindById(id);
            //verifico que existe el registro 
            if (this.repository.FindById(id) != null)
            {
                this.repository.Delete(id);
                return new Result(true, null, data);
            }
            else
            {
                errores.Add(new Error(1, "No existe el id que se quiere eliminar"));
                return new Result(false, errores, null);
            }
        }

        public List<Pledge> FindAll()
        {
            return this.repository.FindAll();
        }

        public Pledge FindById(int id)
        {
            return this.repository.FindById(id);
        }

        public static Result verifyIds(Pledge data)
        {
            int contador = 0;
            List<Error> errores = new List<Error>();
            if (Repository_organization.FindById((int)data.OrganizationId) == null)
            {
                contador++;
                errores.Add(new Error(contador, "No existe el organization id"));
            }
            else if (Repository_constituent.FindById((int)data.ConstituentId) == null)
            {
                contador++;
                errores.Add(new Error(contador, "No existe el constituent id"));
            }
            else if (Repository_solicitor.FindById((int)data.SolicitorId) == null)
            {
                contador++;
                errores.Add(new Error(contador, "No existe el solicitor id"));
            }
            else if (Repository_appeal.FindById((int)data.AppealId) == null)
            {
                contador++;
                errores.Add(new Error(contador, "No existe appeal id"));
            }
            else if (Repository_tribute.FindById((int)data.TributeId) == null)
            {
                contador++;
                errores.Add(new Error(contador, "No existe tribute id"));
            }
            

            if (contador > 0)
            {
                return new Result(false, errores, null);
            }
            else
            {
                return null;

            }
        }
    }
}
