﻿using Proyecto.Filters;
using ProyectConsola.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ProyectConsola.Repository;

namespace ProyectConsola.Services
{
    public static class VerifyId
    {
        public static void GetProperties(Type data)
        {
            List<string> listId = new List<string>();
            listId.Add("AcknowledgementId");
            listId.Add("AppealId");
            listId.Add("CampaignId");
            listId.Add("ConstituentId");
            listId.Add("ContributionId");
            listId.Add("ContributionLUTId");
            listId.Add("ContributionMaintenanceId");
            listId.Add("CustomFieldContributionId");
            listId.Add("DesignationId");
            listId.Add("EventId");
            listId.Add("FundAllocationId");
            listId.Add("MailingListRecipientHistoryId");
            listId.Add("OrganizationId");
            listId.Add("PaymentMethodId");
            listId.Add("PledgeId");
            listId.Add("ReceiptId");
            listId.Add("ScheduledPledgePaymentId");
            listId.Add("ShippingAddressId");
            listId.Add("SoftCreditId");
            listId.Add("SolicitorId");
            listId.Add("TributeId");

            PropertyInfo[] propertyInfo = data.GetProperties();

            foreach (var property in listId)
            {
                foreach (var value in propertyInfo)
                {
                    if (property.Equals(value.Name))
                    {
                        Console.WriteLine(value.GetValue(value));
                    }
                }
            }
        }

    }
}
