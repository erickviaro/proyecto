﻿using ProyectConsola.Interfaces;
using Proyecto.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Services
{
    public class DonationService
    {
        IAcknowledgement acknowledgement;
        IAppeal appeal;
        ICampaign campaign;
        IConstituent constituent;
        IContribution contribution;
        IContributionLUT contributionLUT;
        IContributionMaintenance contributionMaintenance;
        ICustomFieldContributionAnswer customFieldContributionAnswer;
        IDesignation designation;
        IEvent r_event;
        IFundAllocation fundAllocation;
        IMailingListRecipientHistory MailingListRecipientHistory;
        IOrganization organization;
        IPaymentMethod paymentMethod;
        IPledge pledge;
        IReceipt receipt;
        IScheduledPledgePayment scheduledPledgePayment;
        IShippingAddress shippingAddress;
        ISoftCredit softCredit;
        ISolicitor solicitor;
        ITribute tribute;

        public DonationService(IAcknowledgement acknowledgement, IAppeal appeal, ICampaign campaign, IConstituent constituent, IContribution contribution, IContributionLUT contributionLUT, IContributionMaintenance contributionMaintenance, ICustomFieldContributionAnswer customFieldContributionAnswer, IDesignation designation, IEvent r_event, IFundAllocation fundAllocation, IMailingListRecipientHistory mailingListRecipientHistory, IOrganization organization, IPaymentMethod paymentMethod, IPledge pledge, IReceipt receipt, IScheduledPledgePayment scheduledPledgePayment, IShippingAddress shippingAddress, ISoftCredit softCredit, ISolicitor solicitor, ITribute tribute)
        {
            this.acknowledgement = acknowledgement;
            this.appeal = appeal;
            this.campaign = campaign;
            this.constituent = constituent;
            this.contribution = contribution;
            this.contributionLUT = contributionLUT;
            this.contributionMaintenance = contributionMaintenance;
            this.customFieldContributionAnswer = customFieldContributionAnswer;
            this.designation = designation;
            this.r_event = r_event;
            this.fundAllocation = fundAllocation;
            MailingListRecipientHistory = mailingListRecipientHistory;
            this.organization = organization;
            this.paymentMethod = paymentMethod;
            this.pledge = pledge;
            this.receipt = receipt;
            this.scheduledPledgePayment = scheduledPledgePayment;
            this.shippingAddress = shippingAddress;
            this.softCredit = softCredit;
            this.solicitor = solicitor;
            this.tribute = tribute;
        }
    }
}
