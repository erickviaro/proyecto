﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Services
{
    public class TributeService : ITributeService
    {
        ITribute repository;

        public TributeService(ITribute repository)
        {
            this.repository = repository;
        }

        public Result Add(Tribute data)
        {
            var verifyData = data.isValid();

            if (verifyData.resultado) //verifico que los datos sean validos
            {
                this.repository.Add(data);
                return verifyData;
            }
            else
            {
                //si los datos no son validos
                return verifyData;
            }
        }

        public Result Update(Tribute data)
        {
            var verifyData = data.isValid();

            if (verifyData.resultado) //verifico que los datos sean validos
            {
                //verifico que existe el registro 
                if (this.repository.FindById((int)data.TributeId) != null)
                {
                    this.repository.Update(data);
                    return verifyData;
                }
                else
                {
                    verifyData.listaErrores.Add(new Error(1, "No existe el registro que se quiere actualizar."));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
            }
            else
            {
                //si los datos no son validos
                return verifyData;
            }
        }

        public Result Delete(Tribute data)
        {
            var verifyData = data.isValid();

            if (verifyData.resultado) //verifico que los datos sean validos
            {
                //verifico que existe el registro 
                if (this.repository.FindById((int)data.TributeId) != null)
                {
                    this.repository.Delete(data);
                    return verifyData;
                }
                else
                {
                    verifyData.listaErrores.Add(new Error(1, "No existe el registro que se quiere eliminar."));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
            }
            else
            {
                //si los datos no son validos
                return verifyData;
            }
        }

        public Result Delete(int id)
        {
            List<Error> errores = new List<Error>();
            var data = this.repository.FindById(id);
            //verifico que existe el registro 
            if (this.repository.FindById(id) != null)
            {
                this.repository.Delete(id);
                return new Result(true, null, data);
            }
            else
            {
                errores.Add(new Error(1, "No existe el id que se quiere eliminar"));
                return new Result(false, errores, null);
            }
        }

        public List<Tribute> FindAll()
        {
            return this.repository.FindAll();
        }

        public Tribute FindById(int id)
        {
            return this.repository.FindById(id);
        }
    }
}
