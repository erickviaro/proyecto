﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Services
{
    public class ScheduledPledgePaymentService : IScheduledPledgePaymentService
    {
        IScheduledPledgePayment repository;
        IPledge Repository_pledge;
        IContribution Repository_contribution;

        public ScheduledPledgePaymentService(IScheduledPledgePayment repository, IPledge repository_pledge, IContribution repository_contribution)
        {
            this.repository = repository;
            Repository_pledge = repository_pledge;
            Repository_contribution = repository_contribution;
        }

        public Result Add(ScheduledPledgePayment data)
        {
            var verifyData = data.isValid();

            if (verifyData.resultado) //verifico que los datos sean validos
            {
                //verifico que existe organization id
                if (this.Repository_pledge.FindById((int)data.PledgeId) == null)
                {
                    verifyData.listaErrores.Add(new Error(1, "Debe existir Pledge id"));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
                else if (this.Repository_contribution.FindById((int)data.ContributionId) == null)
                {
                    verifyData.listaErrores.Add(new Error(1, "Debe existir Contribution id"));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
                else
                {
                    this.repository.Add(data);
                    return verifyData;
                }
            }
            else
            {
                //si los datos no son validos
                return verifyData;
            }
        }

        public Result Update(ScheduledPledgePayment data)
        {
            var verifyData = data.isValid();

            if (verifyData.resultado) //verifico que los datos sean validos
            {
                //verifico que existe el registro 
                if (this.Repository_pledge.FindById((int)data.PledgeId) == null)
                {
                    verifyData.listaErrores.Add(new Error(1, "Debe existir Pledge id"));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
                else if (this.Repository_contribution.FindById((int)data.ContributionId) == null)
                {
                    verifyData.listaErrores.Add(new Error(1, "Debe existir Contribution id"));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
                else if (this.repository.FindById((int)data.ScheduledPledgePaymentId) == null)
                {
                    verifyData.listaErrores.Add(new Error(1, "No existe el registro que se quiere actualizar."));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
                else
                {
                    this.repository.Update(data);
                    return verifyData;
                }
            }
            else
            {
                //si los datos no son validos
                return verifyData;
            }
        }

        public Result Delete(ScheduledPledgePayment data)
        {
            var verifyData = data.isValid();

            if (verifyData.resultado) //verifico que los datos sean validos
            {
                //verifico que existe el registro 
                if (this.repository.FindById((int)data.ScheduledPledgePaymentId) != null)
                {
                    this.repository.Delete(data);
                    return verifyData;
                }
                else
                {
                    verifyData.listaErrores.Add(new Error(1, "No existe el registro que se quiere eliminar."));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
            }
            else
            {
                //si los datos no son validos
                return verifyData;
            }
        }

        public Result Delete(int id)
        {
            List<Error> errores = new List<Error>();
            var data = this.repository.FindById(id);
            //verifico que existe el registro 
            if (this.repository.FindById(id) != null)
            {
                this.repository.Delete(id);
                return new Result(true, null, data);
            }
            else
            {
                errores.Add(new Error(1, "No existe el id que se quiere eliminar"));
                return new Result(false, errores, null);
            }
        }

        public List<ScheduledPledgePayment> FindAll()
        {
            return this.repository.FindAll();
        }

        public ScheduledPledgePayment FindById(int id)
        {
            return this.repository.FindById(id);
        }
    }
}
