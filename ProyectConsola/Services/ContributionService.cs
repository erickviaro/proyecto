﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Services
{
    public class ContributionService
    {
        IContribution repository;
        static IOrganization Repository_organization;
        static IConstituent Repository_consituent;
        static IPaymentMethod Repository_paymentMethod;
        static IEvent Repository_event;
        static IAcknowledgement Repository_acknowledgement;
        static ICampaign Repository_campaign;
        static IAppeal Repository_appeal;
        static ITribute Repository_tribute;
        static IDesignation Repository_designation;
        static ISolicitor Repository_solicitor;

        public ContributionService(IContribution repository, IOrganization repository_organization, IConstituent repository_consituent, IPaymentMethod repository_paymentMethod, IEvent repository_event, IAcknowledgement repository_acknowledgement, ICampaign repository_campaign, IAppeal repository_appeal, ITribute repository_tribute, IDesignation repository_designation, ISolicitor repository_solicitor)
        {
            this.repository = repository;
            Repository_organization = repository_organization;
            Repository_consituent = repository_consituent;
            Repository_paymentMethod = repository_paymentMethod;
            Repository_event = repository_event;
            Repository_acknowledgement = repository_acknowledgement;
            Repository_campaign = repository_campaign;
            Repository_appeal = repository_appeal;
            Repository_tribute = repository_tribute;
            Repository_designation = repository_designation;
            Repository_solicitor = repository_solicitor;
        }

        public Result Add(Contribution data)
        {
            var verifyData = data.isValid();

            if (verifyData.resultado) //verifico que los datos sean validos
            {
                //verifico que existan ids
                if (verifyIds(data) == null)
                {
                    this.repository.Add(data);
                    return verifyData;
                }
                else
                {
                    return verifyIds(data);
                }
            }
            else
            {
                //si los datos no son validos
                return verifyData;
            }
        }

        public Result Update(Contribution data)
        {
            var verifyData = data.isValid();

            if (verifyData.resultado) //verifico que los datos sean validos
            {
                //verifico que existan ids
                if (verifyIds(data) == null)
                {
                    this.repository.Add(data);
                    return verifyData;
                } 
                //verifico que existe el registro
                if (this.repository.FindById((int)data.ContributionId) == null)
                {
                    verifyData.listaErrores.Add(new Error(1, "No existe el registro que se quiere actualizar."));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
                else
                {
                    this.repository.Update(data);
                    return verifyData;
                }
            }
            else
            {
                //si los datos no son validos
                return verifyData;
            }
        }

        public Result Delete(Contribution data)
        {
            var verifyData = data.isValid();

            if (verifyData.resultado) //verifico que los datos sean validos
            {
                //verifico que existe el registro 
                if (this.repository.FindById((int)data.AcknowledgementId) != null)
                {
                    this.repository.Delete(data);
                    return verifyData;
                }
                else
                {
                    verifyData.listaErrores.Add(new Error(1, "No existe el registro que se quiere eliminar."));
                    verifyData.resultado = false;
                    verifyData.generic = null;
                    return verifyData;
                }
            }
            else
            {
                //si los datos no son validos
                return verifyData;
            }
        }

        public Result Delete(int id)
        {
            List<Error> errores = new List<Error>();
            var data = this.repository.FindById(id);
            //verifico que existe el registro 
            if (this.repository.FindById(id) != null)
            {
                this.repository.Delete(id);
                return new Result(true, null, data);
            }
            else
            {
                errores.Add(new Error(1, "No existe el id que se quiere eliminar"));
                return new Result(false, errores, null);
            }
        }

        public List<Contribution> FindAll()
        {
            return this.repository.FindAll();
        }

        public Contribution FindById(int id)
        {
            return this.repository.FindById(id);
        }

        public static Result verifyIds(Contribution data)
        {
            int contador = 0;
            List<Error> errores = new List<Error>();
            if(Repository_organization.FindById((int)data.OrganizationId) == null)
            {
                contador++;
                errores.Add(new Error(contador, "No existe el organization id"));
            }else if(Repository_consituent.FindById((int)data.ConstituentId) == null)
            {
                contador++;
                errores.Add(new Error(contador, "No existe el constituent id"));
            }
            else if (Repository_paymentMethod.FindById((int)data.PaymentMethod) == null)
            {
                contador++;
                errores.Add(new Error(contador, "No existe paymentMethod id"));
            }
            else if (Repository_event.FindById((int)data.EventId) == null)
            {
                contador++;
                errores.Add(new Error(contador, "No existe event id"));
            }
            else if (Repository_acknowledgement.FindById((int)data.AcknowledgementId) == null)
            {
                contador++;
                errores.Add(new Error(contador, "No existe acknowledgement id"));
            }
            else if (Repository_campaign.FindById((int)data.CampaignId) == null)
            {
                contador++;
                errores.Add(new Error(contador, "No existe campaign id"));
            }
            else if (Repository_appeal.FindById((int)data.AppealId) == null)
            {
                contador++;
                errores.Add(new Error(contador, "No existe appeal id"));
            }
            else if (Repository_tribute.FindById((int)data.TributeId) == null)
            {
                contador++;
                errores.Add(new Error(contador, "No existe tribute id"));
            }
            else if (Repository_designation.FindById((int)data.DesignationId) == null)
            {
                contador++;
                errores.Add(new Error(contador, "No existe designation id"));
            }

            if(contador > 0)
            {
                return new Result(false, errores, null);
            }
            else
            {
                return null;

            }
        }

    }
}
