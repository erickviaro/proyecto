﻿using ProyectConsola.Services;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IContributionLUTService
    {
        Result Add(ContributionLUT data);
        Result Update(ContributionLUT data);
        Result Delete(ContributionLUT data);
        Result Delete(int id);
        List<ContributionLUT> FindAll();
        ContributionLUT FindById(int id);
    }
}
