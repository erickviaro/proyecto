﻿using ProyectConsola.Services;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    interface IDonation
    {
        List<DonationService> FindAll();
        DonationService FindById(int id);
        DonationService Add(DonationService data);
        void Delete(int id);
        void Delete(DonationService data);
        DonationService Update(DonationService data);
    }
}
