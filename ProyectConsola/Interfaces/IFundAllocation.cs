﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IFundAllocation
    {
        List<FundAllocation> FindAll();
        FundAllocation FindById(int id);
        FundAllocation Add(FundAllocation data);
        void Delete(int id);
        void Delete(FundAllocation data);
        FundAllocation Update(FundAllocation data);
    }
}
