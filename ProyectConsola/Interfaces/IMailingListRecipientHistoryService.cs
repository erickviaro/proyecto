﻿using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IMailingListRecipientHistoryService
    {
        Result Add(MailingListRecipientHistory data);
        Result Update(MailingListRecipientHistory data);
        Result Delete(MailingListRecipientHistory data);
        Result Delete(int id);
        List<MailingListRecipientHistory> FindAll();
        MailingListRecipientHistory FindById(int id);
    }
}
