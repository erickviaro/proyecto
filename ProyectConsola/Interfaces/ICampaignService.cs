﻿using ProyectConsola.Services;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface ICampaignService
    {
        Result Add(Campaign data);
        Result Update(Campaign data);
        Result Delete(Campaign data);
        Result Delete(int id);
        List<Campaign> FindAll();
        Campaign FindById(int id);
    }
}
