﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IAppeal
    {
        List<Appeal> FindAll();
        Appeal FindById(int id);
        Appeal Add(Appeal data);
        void Delete(int id);
        void Delete(Appeal data);
        Appeal Update(Appeal data);
    }
}
