﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface ISolicitor
    {
        List<Solicitor> FindAll();
        Solicitor FindById(int id);
        Solicitor Add(Solicitor data);
        void Delete(int id);
        void Delete(Solicitor data);
        Solicitor Update(Solicitor data);
    }
}
