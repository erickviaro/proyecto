﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IConstituent
    {
        List<Constituent> FindAll();
        Constituent FindById(int id);
        Constituent Add(Constituent data);
        void Delete(int id);
        void Delete(Constituent data);
        Constituent Update(Constituent data);
    }
}
