﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IPledge
    {
        List<Pledge> FindAll();
        Pledge FindById(int id);
        Pledge Add(Pledge data);
        void Delete(int id);
        void Delete(Pledge data);
        Pledge Update(Pledge data);
    }
}
