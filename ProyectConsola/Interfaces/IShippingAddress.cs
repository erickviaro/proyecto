﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Interfaces
{
    public interface IShippingAddress
    {
        List<ShippingAddress> FindAll();
        ShippingAddress FindById(int id);
        ShippingAddress Add(ShippingAddress data);
        void Delete(int id);
        void Delete(ShippingAddress data);
        ShippingAddress Update(ShippingAddress data);
    }
}
