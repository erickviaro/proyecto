﻿using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IOrganizationService
    {
        Result Add(Organization data);
        Result Update(Organization data);
        Result Delete(Organization data);
        Result Delete(int id);
        List<Organization> FindAll();
        Organization FindById(int id);
    }
}
