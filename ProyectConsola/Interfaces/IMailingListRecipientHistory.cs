﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IMailingListRecipientHistory
    {
        List<MailingListRecipientHistory> FindAll();
        MailingListRecipientHistory FindById(int id);
        MailingListRecipientHistory Add(MailingListRecipientHistory data);
        void Delete(int id);
        void Delete(MailingListRecipientHistory data);
        MailingListRecipientHistory Update(MailingListRecipientHistory data);
    }
}
