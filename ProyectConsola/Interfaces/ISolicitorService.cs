﻿using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface ISolicitorService
    {
        Result Add(Solicitor data);
        Result Update(Solicitor data);
        Result Delete(Solicitor data);
        Result Delete(int id);
        List<Solicitor> FindAll();
        Solicitor FindById(int id);
    }
}
