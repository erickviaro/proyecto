﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IOrganization
    {
        List<Organization> FindAll();
        Organization FindById(int id);
        Organization Add(Organization data);
        void Delete(int id);
        void Delete(Organization data);
        Organization Update(Organization data);
    }
}
