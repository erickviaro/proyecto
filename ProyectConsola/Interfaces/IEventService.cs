﻿using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IEventService
    {
        Result Add(Event data);
        Result Update(Event data);
        Result Delete(Event data);
        Result Delete(int id);
        List<Event> FindAll();
        Event FindById(int id);
    }
}
