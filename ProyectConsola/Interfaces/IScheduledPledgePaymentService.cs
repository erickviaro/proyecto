﻿using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IScheduledPledgePaymentService
    {
        Result Add(ScheduledPledgePayment data);
        Result Update(ScheduledPledgePayment data);
        Result Delete(ScheduledPledgePayment data);
        Result Delete(int id);
        List<ScheduledPledgePayment> FindAll();
        ScheduledPledgePayment FindById(int id);
    }
}
