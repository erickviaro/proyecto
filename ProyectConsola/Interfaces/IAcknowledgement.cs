﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IAcknowledgement
    {
        List<Acknowledgement> FindAll();
        Acknowledgement FindById(int id);
        Acknowledgement Add(Acknowledgement data);
        void Delete(int id);
        void Delete(Acknowledgement data);
        Acknowledgement Update(Acknowledgement data);
    }
}
