﻿using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface ISoftCreditService
    {
        Result Add(SoftCredit data);
        Result Update(SoftCredit data);
        Result Delete(SoftCredit data);
        Result Delete(int id);
        List<SoftCredit> FindAll();
        SoftCredit FindById(int id);
    }
}
