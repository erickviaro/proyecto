﻿using ProyectConsola.Services;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IDesignationService
    {
        Result Add(Designation data);
        Result Update(Designation data);
        Result Delete(Designation data);
        Result Delete(int id);
        List<Designation> FindAll();
        Designation FindById(int id);
    }
}
