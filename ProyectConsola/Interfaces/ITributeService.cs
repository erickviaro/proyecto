﻿using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface ITributeService
    {
        Result Add(Tribute data);
        Result Update(Tribute data);
        Result Delete(Tribute data);
        Result Delete(int id);
        List<Tribute> FindAll();
        Tribute FindById(int id);
    }
}
