﻿using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IDonationService
    {
        Result Add(Donation data);
        Result Update(Donation data);
        Result Delete(Donation data);
        Result Delete(int id);
        List<Donation> FindAll();
        Donation FindById(int id);
    }
}
