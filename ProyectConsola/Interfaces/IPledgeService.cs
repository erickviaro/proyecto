﻿using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IPledgeService
    {
        Result Add(Pledge data);
        Result Update(Pledge data);
        Result Delete(Pledge data);
        Result Delete(int id);
        List<Pledge> FindAll();
        Pledge FindById(int id);
    }
}
