﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface ITribute
    {
        List<Tribute> FindAll();
        Tribute FindById(int id);
        Tribute Add(Tribute data);
        void Delete(int id);
        void Delete(Tribute data);
        Tribute Update(Tribute data);
    }
}
