﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IEvent
    {
        List<Event> FindAll();
        Event FindById(int id);
        Event Add(Event data);
        void Delete(int id);
        void Delete(Event data);
        Event Update(Event data);
    }
}
