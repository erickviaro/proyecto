﻿using ProyectConsola.Services;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface ICustomFieldContributionAnswerService
    {
        Result Add(CustomFieldContributionAnswer data);
        Result Update(CustomFieldContributionAnswer data);
        Result Delete(CustomFieldContributionAnswer data);
        Result Delete(int id);
        List<CustomFieldContributionAnswer> FindAll();
        CustomFieldContributionAnswer FindById(int id);
    }
}
