﻿using ProyectConsola.Services;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IContributionMaintenanceService
    {
        Result Add(ContributionMaintenance data);
        Result Update(ContributionMaintenance data);
        Result Delete(ContributionMaintenance data);
        Result Delete(int id);
        List<ContributionMaintenance> FindAll();
        ContributionMaintenance FindById(int id);
    }
}
