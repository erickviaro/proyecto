﻿using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IShippingAddressService
    {
        Result Add(ShippingAddress data);
        Result Update(ShippingAddress data);
        Result Delete(ShippingAddress data);
        Result Delete(int id);
        List<ShippingAddress> FindAll();
        ShippingAddress FindById(int id);
    }
}
