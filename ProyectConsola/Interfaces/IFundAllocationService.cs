﻿using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IFundAllocationService
    {
        Result Add(FundAllocation data);
        Result Update(FundAllocation data);
        Result Delete(FundAllocation data);
        Result Delete(int id);
        List<FundAllocation> FindAll();
        FundAllocation FindById(int id);
    }
}
