﻿using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IPaymentMethodService
    {
        Result Add(PaymentMethod data);
        Result Update(PaymentMethod data);
        Result Delete(PaymentMethod data);
        Result Delete(int id);
        List<PaymentMethod> FindAll();
        PaymentMethod FindById(int id);
    }
}
