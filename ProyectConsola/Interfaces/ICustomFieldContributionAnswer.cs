﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface ICustomFieldContributionAnswer
    {
        List<CustomFieldContributionAnswer> FindAll();
        CustomFieldContributionAnswer FindById(int id);
        CustomFieldContributionAnswer Add(CustomFieldContributionAnswer data);
        void Delete(int id);
        void Delete(CustomFieldContributionAnswer data);
        CustomFieldContributionAnswer Update(CustomFieldContributionAnswer data);
    }
}
