﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IContributionMaintenance
    {
        List<ContributionMaintenance> FindAll();
        ContributionMaintenance FindById(int id);
        ContributionMaintenance Add(ContributionMaintenance data);
        void Delete(int id);
        void Delete(ContributionMaintenance data);
        ContributionMaintenance Update(ContributionMaintenance data);
    }
}
