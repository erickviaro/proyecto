﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IContribution
    {
        List<Contribution> FindAll();
        Contribution FindById(int id);
        Contribution Add(Contribution data);
        void Delete(int id);
        void Delete(Contribution data);
        Contribution Update(Contribution data);
    }
}
