﻿using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IAcknowledgementService
    {
        Result Add(Acknowledgement data);
        Result Update(Acknowledgement data);
        Result Delete(Acknowledgement data);
        Result Delete(int id);
        List<Acknowledgement> FindAll();
        Acknowledgement FindById(int id);
    }
}
