﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IScheduledPledgePayment
    {
        List<ScheduledPledgePayment> FindAll();
        ScheduledPledgePayment FindById(int id);
        ScheduledPledgePayment Add(ScheduledPledgePayment data);
        void Delete(int id);
        void Delete(ScheduledPledgePayment data);
        ScheduledPledgePayment Update(ScheduledPledgePayment data);
    }
}
