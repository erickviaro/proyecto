﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface ISoftCredit
    {
        List<SoftCredit> FindAll();
        SoftCredit FindById(int id);
        SoftCredit Add(SoftCredit data);
        void Delete(int id);
        void Delete(SoftCredit data);
        SoftCredit Update(SoftCredit data);
    }
}
