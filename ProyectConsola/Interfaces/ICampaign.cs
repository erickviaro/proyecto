﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface ICampaign
    {
        List<Campaign> FindAll();
        Campaign FindById(int id);
        Campaign Add(Campaign data);
        void Delete(int id);
        void Delete(Campaign data);
        Campaign Update(Campaign data);
    }
}
