﻿using ProyectConsola.Services;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IConstituentService
    {
        Result Add(Constituent data);
        Result Update(Constituent data);
        Result Delete(Constituent data);
        Result Delete(int id);
        List<Constituent> FindAll();
        Constituent FindById(int id);
    }
}
