﻿using ProyectConsola.Services;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IContributionService
    {
        Result Add(Contribution data);
        Result Update(Contribution data);
        Result Delete(Contribution data);
        Result Delete(int id);
        List<Contribution> FindAll();
        Contribution FindById(int id);
    }
}
