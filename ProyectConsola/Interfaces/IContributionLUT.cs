﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IContributionLUT
    {
        List<ContributionLUT> FindAll();
        ContributionLUT FindById(int id);
        ContributionLUT Add(ContributionLUT data);
        void Delete(int id);
        void Delete(ContributionLUT data);
        ContributionLUT Update(ContributionLUT data);
    }
}
