﻿using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IReceiptService
    {
        Result Add(Receipt data);
        Result Update(Receipt data);
        Result Delete(Receipt data);
        Result Delete(int id);
        List<Receipt> FindAll();
        Receipt FindById(int id);
    }
}
