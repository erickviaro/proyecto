﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IDesignation
    {
        List<Designation> FindAll();
        Designation FindById(int id);
        Designation Add(Designation data);
        void Delete(int id);
        void Delete(Designation data);
        Designation Update(Designation data);
    }
}
