﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IReceipt
    {
        List<Receipt> FindAll();
        Receipt FindById(int id);
        Receipt Add(Receipt data);
        void Delete(int id);
        void Delete(Receipt data);
        Receipt Update(Receipt data);
    }
}
