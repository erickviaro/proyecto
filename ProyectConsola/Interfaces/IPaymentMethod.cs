﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Interfaces
{
    public interface IPaymentMethod
    {
        List<PaymentMethod> FindAll();
        PaymentMethod FindById(int id);
        PaymentMethod Add(PaymentMethod data);
        void Delete(int id);
        void Delete(PaymentMethod data);
        PaymentMethod Update(PaymentMethod data);
    }
}
