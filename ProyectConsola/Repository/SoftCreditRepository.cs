﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class SoftCreditRepository : ISoftCredit
    {
        List<SoftCredit> list = new List<SoftCredit>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public SoftCredit Add(SoftCredit data)
        {
            data.SoftCreditId = contadorId;
            list.Add(data);
            contadorId++;
            return data;
        }

        public SoftCredit Update(SoftCredit data)
        {
            var verify = FindById((int)data.SoftCreditId);
            int idToReplace = list.IndexOf(verify);
            list[idToReplace] = data;
            return list[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            list.Remove(verify);
        }

        public void Delete(SoftCredit data)
        {
            var verify = FindById((int)data.SoftCreditId);
            list.Remove(verify);
        }

        public List<SoftCredit> FindAll()
        {
            return list.ToList();
        }

        public SoftCredit FindById(int id)
        {
            var answer = list.Find(c => c.SoftCreditId == id);
            return answer;
        }
    }
}
