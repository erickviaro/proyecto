﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class ContributionRepository : IContribution
    {
        List<Contribution> contributionList = new List<Contribution>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public Contribution Add(Contribution data)
        {
            data.ContributionId = contadorId;
            contributionList.Add(data);
            contadorId++;
            return data;
        }

        public Contribution Update(Contribution data)
        {
            var verify = FindById((int)data.ContributionId);
            int idToReplace = contributionList.IndexOf(verify);
            contributionList[idToReplace] = data;
            return contributionList[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            contributionList.Remove(verify);
        }

        public void Delete(Contribution data)
        {
            var verify = FindById((int)data.ContributionId);
            contributionList.Remove(verify);
        }

        public List<Contribution> FindAll()
        {
            return contributionList.ToList();
        }

        public Contribution FindById(int id)
        {
            var answer = contributionList.Find(c => c.ContributionId == id);
            return answer;
        }
    }
}
