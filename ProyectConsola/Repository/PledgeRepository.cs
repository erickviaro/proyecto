﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class PledgeRepository : IPledge
    {
        List<Pledge> list = new List<Pledge>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public Pledge Add(Pledge data)
        {
            data.PledgeId = contadorId;
            list.Add(data);
            contadorId++;
            return data;
        }

        public Pledge Update(Pledge data)
        {
            var verify = FindById((int)data.PledgeId);
            int idToReplace = list.IndexOf(verify);
            list[idToReplace] = data;
            return list[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            list.Remove(verify);
        }

        public void Delete(Pledge data)
        {
            var verify = FindById((int)data.PledgeId);
            list.Remove(verify);
        }

        public List<Pledge> FindAll()
        {
            return list.ToList();
        }

        public Pledge FindById(int id)
        {
            var answer = list.Find(c => c.PledgeId == id);
            return answer;
        }
    }
}
