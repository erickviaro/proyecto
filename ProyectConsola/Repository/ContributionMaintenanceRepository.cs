﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    class ContributionMaintenanceRepository : IContributionMaintenance
    {
        List<ContributionMaintenance> contributionMaintenanceList = new List<ContributionMaintenance>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public ContributionMaintenance Add(ContributionMaintenance data)
        {
            data.ContributionId = contadorId;
            contributionMaintenanceList.Add(data);
            contadorId++;
            return data;
        }

        public ContributionMaintenance Update(ContributionMaintenance data)
        {
            var verify = FindById((int)data.ContributionId);
            int idToReplace = contributionMaintenanceList.IndexOf(verify);
            contributionMaintenanceList[idToReplace] = data;
            return contributionMaintenanceList[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            contributionMaintenanceList.Remove(verify);
        }

        public void Delete(ContributionMaintenance data)
        {
            var verify = FindById((int)data.ContributionId);
            contributionMaintenanceList.Remove(verify);
        }

        public List<ContributionMaintenance> FindAll()
        {
            return contributionMaintenanceList.ToList();
        }

        public ContributionMaintenance FindById(int id)
        {
            var answer = contributionMaintenanceList.Find(c => c.ContributionId == id);
            return answer;
        }
    }
}
