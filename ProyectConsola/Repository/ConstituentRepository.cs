﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class ConstituentRepository : IConstituent
    {
        List<Constituent> constituentList = new List<Constituent>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public Constituent Add(Constituent data)
        {
            data.ConstituentId = contadorId;
            constituentList.Add(data);
            contadorId++;
            return data;
        }

        public Constituent Update(Constituent data)
        {
            var verify = FindById((int)data.ConstituentId);
            int idToReplace = constituentList.IndexOf(verify);
            constituentList[idToReplace] = data;
            return constituentList[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            constituentList.Remove(verify);
        }

        public void Delete(Constituent data)
        {
            var verify = FindById((int)data.ConstituentId);
            constituentList.Remove(verify);
        }

        public List<Constituent> FindAll()
        {
            return constituentList.ToList();
        }

        public Constituent FindById(int id)
        {
            var answer = constituentList.Find(c => c.ConstituentId == id);
            return answer;
        }
    }
}
