﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class SchedulePledgePaymentRepository : IScheduledPledgePayment
    {
        List<ScheduledPledgePayment> list = new List<ScheduledPledgePayment>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public ScheduledPledgePayment Add(ScheduledPledgePayment data)
        {
            data.ScheduledPledgePaymentId = contadorId;
            list.Add(data);
            contadorId++;
            return data;
        }

        public ScheduledPledgePayment Update(ScheduledPledgePayment data)
        {
            var verify = FindById((int)data.ScheduledPledgePaymentId);
            int idToReplace = list.IndexOf(verify);
            list[idToReplace] = data;
            return list[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            list.Remove(verify);
        }

        public void Delete(ScheduledPledgePayment data)
        {
            var verify = FindById((int)data.ScheduledPledgePaymentId);
            list.Remove(verify);
        }

        public List<ScheduledPledgePayment> FindAll()
        {
            return list.ToList();
        }

        public ScheduledPledgePayment FindById(int id)
        {
            var answer = list.Find(c => c.ScheduledPledgePaymentId == id);
            return answer;
        }
    }
}
