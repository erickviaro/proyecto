﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class TributeRepository : ITribute
    {
        List<Tribute> list = new List<Tribute>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public Tribute Add(Tribute data)
        {
            data.TributeId = contadorId;
            list.Add(data);
            contadorId++;
            return data;
        }

        public Tribute Update(Tribute data)
        {
            var verify = FindById((int)data.TributeId);
            int idToReplace = list.IndexOf(verify);
            list[idToReplace] = data;
            return list[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            list.Remove(verify);
        }

        public void Delete(Tribute data)
        {
            var verify = FindById((int)data.TributeId);
            list.Remove(verify);
        }

        public List<Tribute> FindAll()
        {
            return list.ToList();
        }

        public Tribute FindById(int id)
        {
            var answer = list.Find(c => c.TributeId == id);
            return answer;
        }
    }
}
