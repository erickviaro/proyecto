﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class CustomFieldContributionAnswerRepository : ICustomFieldContributionAnswer
    {
        List<CustomFieldContributionAnswer> list = new List<CustomFieldContributionAnswer>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public CustomFieldContributionAnswer Add(CustomFieldContributionAnswer data)
        {
            data.CustomFieldContributionId = contadorId;
            list.Add(data);
            contadorId++;
            return data;
        }

        public CustomFieldContributionAnswer Update(CustomFieldContributionAnswer data)
        {
            var verify = FindById((int)data.CustomFieldContributionId);
            int idToReplace = list.IndexOf(verify);
            list[idToReplace] = data;
            return list[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            list.Remove(verify);
        }

        public void Delete(CustomFieldContributionAnswer data)
        {
            var verify = FindById((int)data.CustomFieldContributionId);
            list.Remove(verify);
        }

        public List<CustomFieldContributionAnswer> FindAll()
        {
            return list.ToList();
        }

        public CustomFieldContributionAnswer FindById(int id)
        {
            var answer = list.Find(c => c.CustomFieldContributionId == id);
            return answer;
        }
    }
}
