﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class OrganizationRepository : IOrganization
    {
        List<Organization> list = new List<Organization>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public Organization Add(Organization data)
        {
            data.OrganizationId = contadorId;
            list.Add(data);
            contadorId++;
            return data;
        }

        public Organization Update(Organization data)
        {
            var verify = FindById((int)data.OrganizationId);
            int idToReplace = list.IndexOf(verify);
            list[idToReplace] = data;
            return list[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            list.Remove(verify);
        }

        public void Delete(Organization data)
        {
            var verify = FindById((int)data.OrganizationId);
            list.Remove(verify);
        }

        public List<Organization> FindAll()
        {
            return list.ToList();
        }

        public Organization FindById(int id)
        {
            var answer = list.Find(c => c.OrganizationId == id);
            return answer;
        }
    }
}
