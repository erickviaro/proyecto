﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class SolicitorRepository : ISolicitor
    {
        List<Solicitor> list = new List<Solicitor>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public Solicitor Add(Solicitor data)
        {
            data.SolicitorId = contadorId;
            list.Add(data);
            contadorId++;
            return data;
        }

        public Solicitor Update(Solicitor data)
        {
            var verify = FindById((int)data.SolicitorId);
            int idToReplace = list.IndexOf(verify);
            list[idToReplace] = data;
            return list[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            list.Remove(verify);
        }

        public void Delete(Solicitor data)
        {
            var verify = FindById((int)data.SolicitorId);
            list.Remove(verify);
        }

        public List<Solicitor> FindAll()
        {
            return list.ToList();
        }

        public Solicitor FindById(int id)
        {
            var answer = list.Find(c => c.SolicitorId == id);
            return answer;
        }
    }
}
