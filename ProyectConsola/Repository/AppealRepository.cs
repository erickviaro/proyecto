﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class AppealRepository : IAppeal
    {
        List<Appeal> appealList = new List<Appeal>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public Appeal Add(Appeal data)
        {
            data.AppealId = contadorId;
            appealList.Add(data);
            contadorId++;
            return data;
        }

        public Appeal Update(Appeal data)
        {
            var verify = FindById((int)data.AppealId);
            verify.AppealOrder = data.AppealOrder;
            verify.Description = data.Description;
            verify.IsVisible = data.IsVisible;
            verify.Name = data.Name;
            verify.OrganizationId = data.OrganizationId;
            return verify;
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            appealList.Remove(verify);
        }

        public void Delete(Appeal data)
        {
            var verify = FindById((int)data.AppealId);
            appealList.Remove(verify);
        }

        public List<Appeal> FindAll()
        {
            return appealList.ToList();
        }

        public Appeal FindById(int id)
        {
            var answer = appealList.Find(c => c.AppealId == id);
            return answer;
        }
    }
}
