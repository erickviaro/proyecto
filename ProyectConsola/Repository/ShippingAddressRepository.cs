﻿using Proyecto.Filters;
using Proyecto.Interfaces;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Proyecto.Repository
{
    public class ShippingAddressRepository : IShippingAddress
    {
        List<ShippingAddress> list = new List<ShippingAddress>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public ShippingAddress Add(ShippingAddress data)
        {
            data.ShippingAddressId = contadorId;
            list.Add(data);
            contadorId++;
            return data;
        }

        public ShippingAddress Update(ShippingAddress data)
        {
            var verify = FindById((int)data.ShippingAddressId);
            int idToReplace = list.IndexOf(verify);
            list[idToReplace] = data;
            return list[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            list.Remove(verify);
        }

        public void Delete(ShippingAddress data)
        {
            var verify = FindById((int)data.ShippingAddressId);
            list.Remove(verify);
        }

        public List<ShippingAddress> FindAll()
        {
            return list.ToList();
        }

        public ShippingAddress FindById(int id)
        {
            var answer = list.Find(c => c.ShippingAddressId == id);
            return answer;
        }
    }
}