﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class CampaignRepository : ICampaign
    {
        List<Campaign> campaignList = new List<Campaign>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public Campaign Add(Campaign data)
        {
            data.CampaignId = contadorId;
            campaignList.Add(data);
            contadorId++;
            return data;
        }

        public Campaign Update(Campaign data)
        {
            var verify = FindById((int)data.CampaignId);
            int idToReplace = campaignList.IndexOf(verify);
            campaignList[idToReplace] = data;
            return campaignList[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            campaignList.Remove(verify);
        }

        public void Delete(Campaign data)
        {
            var verify = FindById((int)data.CampaignId);
            campaignList.Remove(verify);
        }

        public List<Campaign> FindAll()
        {
            return campaignList.ToList();
        }

        public Campaign FindById(int id)
        {
            var answer = campaignList.Find(c => c.CampaignId == id);
            return answer;
        }
    }
}
