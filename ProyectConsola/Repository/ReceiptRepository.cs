﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class ReceiptRepository : IReceipt
    {
        List<Receipt> list = new List<Receipt>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public Receipt Add(Receipt data)
        {
            data.ReceiptId = contadorId;
            list.Add(data);
            contadorId++;
            return data;
        }

        public Receipt Update(Receipt data)
        {
            var verify = FindById((int)data.ReceiptId);
            int idToReplace = list.IndexOf(verify);
            list[idToReplace] = data;
            return list[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            list.Remove(verify);
        }

        public void Delete(Receipt data)
        {
            var verify = FindById((int)data.ReceiptId);
            list.Remove(verify);
        }

        public List<Receipt> FindAll()
        {
            return list.ToList();
        }

        public Receipt FindById(int id)
        {
            var answer = list.Find(c => c.ReceiptId == id);
            return answer;
        }
    }
}
