﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class PaymentMethodRepository : IPaymentMethod
    {
        List<PaymentMethod> list = new List<PaymentMethod>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public PaymentMethod Add(PaymentMethod data)
        {
            data.PaymentMethodId = contadorId;
            list.Add(data);
            contadorId++;
            return data;
        }

        public PaymentMethod Update(PaymentMethod data)
        {
            var verify = FindById((int)data.PaymentMethodId);
            int idToReplace = list.IndexOf(verify);
            list[idToReplace] = data;
            return list[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            list.Remove(verify);
        }

        public void Delete(PaymentMethod data)
        {
            var verify = FindById((int)data.PaymentMethodId);
            list.Remove(verify);
        }

        public List<PaymentMethod> FindAll()
        {
            return list.ToList();
        }

        public PaymentMethod FindById(int id)
        {
            var answer = list.Find(c => c.PaymentMethodId == id);
            return answer;
        }
    }
}
