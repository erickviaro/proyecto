﻿using ProyectConsola.Interfaces;
using Proyecto.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class FundAllocationRepository : IFundAllocation
    {
        List<FundAllocation> list = new List<FundAllocation>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public FundAllocation Add(FundAllocation data)
        {
            data.FundAllocationId = contadorId;
            list.Add(data);
            contadorId++;
            return data;
        }

        public FundAllocation Update(FundAllocation data)
        {
            var verify = FindById((int)data.FundAllocationId);
            int idToReplace = list.IndexOf(verify);
            list[idToReplace] = data;
            return list[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            list.Remove(verify);
        }

        public void Delete(FundAllocation data)
        {
            var verify = FindById((int)data.FundAllocationId);
            list.Remove(verify);
        }

        public List<FundAllocation> FindAll()
        {
            return list.ToList();
        }

        public FundAllocation FindById(int id)
        {
            var answer = list.Find(c => c.FundAllocationId == id);
            return answer;
        }
    }
}
