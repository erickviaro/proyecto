﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class ContributionLUTRepository : IContributionLUT
    {
        List<ContributionLUT> contributionLUTList = new List<ContributionLUT>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public ContributionLUT Add(ContributionLUT data)
        {
            data.ContributionLUTId = contadorId;
            contributionLUTList.Add(data);
            contadorId++;
            return data;
        }

        public ContributionLUT Update(ContributionLUT data)
        {
            var verify = FindById((int)data.ContributionLUTId);
            int idToReplace = contributionLUTList.IndexOf(verify);
            contributionLUTList[idToReplace] = data;
            return contributionLUTList[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            contributionLUTList.Remove(verify);
        }

        public void Delete(ContributionLUT data)
        {
            var verify = FindById((int)data.ContributionLUTId);
            contributionLUTList.Remove(verify);
        }

        public List<ContributionLUT> FindAll()
        {
            return contributionLUTList.ToList();
        }

        public ContributionLUT FindById(int id)
        {
            var answer = contributionLUTList.Find(c => c.ContributionLUTId == id);
            return answer;
        }
    }
}
