﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class MailingListRecipientHistoryRepository : IMailingListRecipientHistory
    {
        List<MailingListRecipientHistory> list = new List<MailingListRecipientHistory>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public MailingListRecipientHistory Add(MailingListRecipientHistory data)
        {
            data.MailingListRecipientHistoryId = contadorId;
            list.Add(data);
            contadorId++;
            return data;
        }

        public MailingListRecipientHistory Update(MailingListRecipientHistory data)
        {
            var verify = FindById((int)data.MailingListRecipientHistoryId);
            int idToReplace = list.IndexOf(verify);
            list[idToReplace] = data;
            return list[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            list.Remove(verify);
        }

        public void Delete(MailingListRecipientHistory data)
        {
            var verify = FindById((int)data.MailingListRecipientHistoryId);
            list.Remove(verify);
        }

        public List<MailingListRecipientHistory> FindAll()
        {
            return list.ToList();
        }

        public MailingListRecipientHistory FindById(int id)
        {
            var answer = list.Find(c => c.MailingListRecipientHistoryId == id);
            return answer;
        }
    }
}
