﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class EventRepository : IEvent
    {
        List<Event> list = new List<Event>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public Event Add(Event data)
        {
            data.EventId = contadorId;
            list.Add(data);
            contadorId++;
            return data;
        }

        public Event Update(Event data)
        {
            var verify = FindById((int)data.EventId);
            int idToReplace = list.IndexOf(verify);
            list[idToReplace] = data;
            return list[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            list.Remove(verify);
        }

        public void Delete(Event data)
        {
            var verify = FindById((int)data.EventId);
            list.Remove(verify);
        }

        public List<Event> FindAll()
        {
            return list.ToList();
        }

        public Event FindById(int id)
        {
            var answer = list.Find(c => c.EventId == id);
            return answer;
        }
    }
}
