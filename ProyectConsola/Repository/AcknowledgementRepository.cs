﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class AcknowledgementRepository : IAcknowledgement
    {
        List<Acknowledgement> acknowledgementList = new List<Acknowledgement>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public Acknowledgement Add(Acknowledgement data)
        {
            data.AcknowledgementId = contadorId;
            acknowledgementList.Add(data);
            contadorId++;
            return data;
        }

        public Acknowledgement Update(Acknowledgement data)
        {
            var verify = FindById((int)data.AcknowledgementId);
            int idToReplace = acknowledgementList.IndexOf(verify);
            acknowledgementList[idToReplace] = data;
            return acknowledgementList[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            acknowledgementList.Remove(verify);
        }

        public void Delete(Acknowledgement data)
        {
            var verify = FindById((int)data.AcknowledgementId);
            acknowledgementList.Remove(verify);
        }

        public List<Acknowledgement> FindAll()
        {
            return acknowledgementList.ToList();
        }

        public Acknowledgement FindById(int id)
        {
            var answer = acknowledgementList.Find(c => c.AcknowledgementId == id);
            return answer;
        }
    }
}
