﻿using ProyectConsola.Interfaces;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectConsola.Repository
{
    public class DesignationRepository : IDesignation
    {
        List<Designation> list = new List<Designation>();
        List<Error> errores = new List<Error>();
        static int contadorId = 1;

        public Designation Add(Designation data)
        {
            data.DesignationId = contadorId;
            list.Add(data);
            contadorId++;
            return data;
        }

        public Designation Update(Designation data)
        {
            var verify = FindById((int)data.DesignationId);
            int idToReplace = list.IndexOf(verify);
            list[idToReplace] = data;
            return list[idToReplace];
        }

        public void Delete(int id)
        {
            var verify = FindById(id);
            list.Remove(verify);
        }

        public void Delete(Designation data)
        {
            var verify = FindById((int)data.DesignationId);
            list.Remove(verify);
        }

        public List<Designation> FindAll()
        {
            return list.ToList();
        }

        public Designation FindById(int id)
        {
            var answer = list.Find(c => c.DesignationId == id);
            return answer;
        }
    }
}
