﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Proyecto.Models
{
    public class MailingListRecipientHistory
    {
        static Configuracion config = new Configuracion(2, 200);
        public int? MailingListRecipientHistoryId { get; set; }
        public int? MailingListHistoryId { get; set; }
        public int? MailingStatus { get; set; }
        public string ConstituentGuid { get; set; }
        public string MailingName { get; set; }
        public string UnsubscribeToken { get; set; }
        public string MailingProperties { get; set; }
        public int? DonationId { get; set; }
        public int? PledgeId { get; set; }
        public int? ConstituentId { get; set; }
        public int? NotifiedConstituentId { get; set; }
        public DateTime ProcessDate { get; set; }

        static DateTime today = DateTime.Now;
        static DateTime initialTime = new DateTime(1, 1, 1);
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public MailingListRecipientHistory(int? mailingListRecipientHistoryId, int? mailingListHistoryId, int? mailingStatus, string constituentGuid, string mailingName, string unsubscribeToken, string mailingProperties, int? donationId, int? pledgeId, int? constituentId, int? notifiedConstituentId, DateTime processDate)
        {
            MailingListRecipientHistoryId = mailingListRecipientHistoryId;
            MailingListHistoryId = mailingListHistoryId;
            MailingStatus = mailingStatus;
            ConstituentGuid = constituentGuid;
            MailingName = mailingName;
            UnsubscribeToken = unsubscribeToken;
            MailingProperties = mailingProperties;
            DonationId = donationId;
            PledgeId = pledgeId;
            ConstituentId = constituentId;
            NotifiedConstituentId = notifiedConstituentId;
            ProcessDate = processDate;
        }

        public MailingListRecipientHistory(int? mailingListHistoryId, int? mailingStatus, string constituentGuid, string mailingName, string unsubscribeToken, string mailingProperties, int? donationId, int? pledgeId, int? constituentId, int? notifiedConstituentId, DateTime processDate)
        {
            MailingListRecipientHistoryId = generateId();
            MailingListHistoryId = mailingListHistoryId;
            MailingStatus = mailingStatus;
            ConstituentGuid = constituentGuid;
            MailingName = mailingName;
            UnsubscribeToken = unsubscribeToken;
            MailingProperties = mailingProperties;
            DonationId = donationId;
            PledgeId = pledgeId;
            ConstituentId = constituentId;
            NotifiedConstituentId = notifiedConstituentId;
            ProcessDate = processDate;
        }

        public MailingListRecipientHistory()
        {
        }

        public Result isValid()
        {
            int contador = 0;
            MailingListRecipientHistory data = new MailingListRecipientHistory(MailingListRecipientHistoryId, MailingListHistoryId, MailingStatus,
                ConstituentGuid, MailingName, UnsubscribeToken, MailingProperties, DonationId, PledgeId, ConstituentId,
                NotifiedConstituentId, ProcessDate);
            if (validateLimitInt((int)MailingListRecipientHistoryId) == 0 ||
                validateLimitInt((int)MailingListHistoryId) == 0 ||
                validateLimitInt((int)MailingStatus) == 0 ||
                validateLimitInt((int)DonationId) == 0 ||
                validateLimitInt((int)PledgeId) == 0 ||
                validateLimitInt((int)ConstituentId) == 0 ||
                validateLimitInt((int)NotifiedConstituentId) == 0
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }
            Match match = config.regex.Match(MailingName);

            if (!match.Success)
            {
                contador++;
                listError.Add(new Error(contador, "Solo se aceptan emails"));

            }

            if (MailingListRecipientHistoryId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingListRecipientHistoryId"));
            }
            if (MailingListHistoryId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingListHistoryId"));
            }
            if (MailingStatus == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingStatus"));
            }
            if (DonationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DonationId"));
            }
            if (PledgeId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta PledgeId"));
            }
            if (ConstituentId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ConstituentId"));
            }
            if (NotifiedConstituentId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta NotifiedConstituentId"));
            }
            if (ProcessDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ProcessDate"));
            }
            if (string.IsNullOrWhiteSpace(ConstituentGuid))
            {
                contador++;
                listError.Add(new Error(contador, "Falta ConstituentGuid"));
            }
            if (string.IsNullOrWhiteSpace(MailingName))
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingName"));
            }
            if (string.IsNullOrWhiteSpace(UnsubscribeToken))
            {
                contador++;
                listError.Add(new Error(contador, "Falta UnsubscribeToken"));
            }
            if (string.IsNullOrWhiteSpace(MailingProperties))
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingProperties"));
            }

            if (verifyDate(ProcessDate) == false)
            {
                contador++;
                listError.Add(new Error(contador, "Las fechas no pueden ser menores de 3 anios."));
            }

            //last validation
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }

        public int generateId()
        {
            return contadorId;
        }

        public override string ToString()
        {
            string info = "MailingListRecipientHistoryId: " + MailingListRecipientHistoryId + "\n" +
                            "MailingListHistoryId: " + MailingListHistoryId + "\n" +
                            "MailingStatus: " + MailingStatus + "\n" +
                            "ConstituentGuid: " + ConstituentGuid + "\n" +
                            "MailingName: " + MailingName + "\n" +
                            "UnsubscribeToken: " + UnsubscribeToken + "\n" +
                            "MailingProperties: " + MailingProperties + "\n" +
                            "DonationId: " + DonationId + "\n" +
                            "PledgeId: " + PledgeId + "\n" +
                            "ConstituentId: " + ConstituentId + "\n" +
                            "NotifiedConstituentId: " + NotifiedConstituentId + "\n" +
                            "ProcessDate: " + ProcessDate + "\n";
            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }

        public static float convertFloat(float value, int digits)
        {
            double result = Math.Round(value, digits);
            return (float)result;
        }

        public static Boolean verifyDate(DateTime date)
        {
            TimeSpan result = today - date;
            int years = (initialTime + result).Year - 1;
            return !(years < 3);
        }
    }
}