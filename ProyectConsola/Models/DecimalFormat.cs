﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class DecimalFormat
    {
        public static float setFormat(float num, int limit)
        {
            string baseFormat = "";
            for(int i = 0; i <= limit; i++)
            {
                baseFormat += "0";
            }
            if(baseFormat != "")
            {
                return float.Parse(String.Format("{0:0." + baseFormat + "}",num));
            }
            else
            {
                return float.Parse(String.Format("{0:0.00}", num));
            }
        }
    }
}