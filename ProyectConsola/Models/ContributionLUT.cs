﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class ContributionLUT
    {
        static Configuracion config = new Configuracion(2, 2000);
        public int? ContributionLUTId { get; set; }
        public int? ContributionId { get; set; }
        public int? IsDeleted { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime LastUpdated { get; set; }
        public string ExternalId { get; set; }
        public string ExternalStatus { get; set; }
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public ContributionLUT(int? contributionLUTId, int? contributionId, int? isDeleted, 
                                 string externalId, string externalStatus)
        {
            ContributionLUTId = contributionLUTId;
            ContributionId = contributionId;
            IsDeleted = isDeleted;
            DateCreated = DateTime.UtcNow.Date; 
            LastUpdated = DateTime.UtcNow.Date; 
            ExternalId = externalId;
            ExternalStatus = externalStatus;
        }

        public ContributionLUT(int? contributionId, int? isDeleted,
            string externalId, string externalStatus)
        {
            ContributionLUTId = generateId();
            ContributionId = contributionId;
            IsDeleted = isDeleted;
            DateCreated = DateTime.UtcNow.Date; 
            LastUpdated = DateTime.UtcNow.Date; 
            ExternalId = externalId;
            ExternalStatus = externalStatus;
        }

        public ContributionLUT()
        {
        }

        public int generateId()
        {
            return contadorId;
        }
        
        public Result isValid()
        {
            int contador = 0;
            ContributionLUT data = new ContributionLUT(ContributionLUTId, ContributionId, IsDeleted, ExternalId, ExternalStatus);
            if (validateLimitInt((int)ContributionLUTId) == 0 ||
                validateLimitInt((int)ContributionId) == 0 ||
                validateLimitInt((int)IsDeleted) == 0 
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }

            if (ContributionLUTId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionLUTId"));
            }
            if (ContributionId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionId"));
            }
            
            if (IsDeleted == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsDeleted"));
            }

            if (DateCreated == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DateCreated"));
            }
            if (LastUpdated == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LastUpdated"));
            }
            if (string.IsNullOrWhiteSpace(ExternalId))
            {
                contador++;
                listError.Add(new Error(contador, "Falta ExternalId"));
            }
            if (string.IsNullOrWhiteSpace(ExternalStatus))
            {
                contador++;
                listError.Add(new Error(contador, "Falta ExternalId"));
            }

            //last verification
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }

        public override string ToString()
        {
            string info = "ContributionLUTId: " + ContributionLUTId + "\n" +
                            "ContributionId: " + ContributionId + "\n" +
                            "IsDeleted: " + IsDeleted + "\n" +
                            "DateCreated: " + DateCreated + "\n" +
                            "LastUpdated: " + LastUpdated + "\n" +
                            "ExternalId: " + ExternalId + "\n" +
                            "ExternalStatus: " + ExternalStatus + "\n";

            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }
    }
}