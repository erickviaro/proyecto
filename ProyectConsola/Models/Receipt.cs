﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class Receipt
    {
        static Configuracion config = new Configuracion(2, 200);
        public int? ReceiptId { get; set; }
        public int? ContributionId { get; set; }
        public int? MailingListId { get; set; }
        public int? ReceiptNumber { get; set; }
        public float? ReceiptedAmount { get; set; }
        public DateTime ReceiptDate { get; set; }
        public int? ReceiptOrder { get; set; }
        public int? ReceiptStatus { get; set; }
        public int? OrganizationId { get; set; }
        public int? LegacyID { get; set; }
        public int? DocumentId { get; set; }

        static DateTime today = DateTime.Now;
        static DateTime initialTime = new DateTime(1, 1, 1);
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public Receipt(int? receiptId, int? contributionId, int? mailingListId, int? receiptNumber, float? receiptedAmount, DateTime receiptDate, int? receiptOrder, int? receiptStatus, int? organizationId, int? legacyID, int? documentId)
        {
            ReceiptId = receiptId;
            ContributionId = contributionId;
            MailingListId = mailingListId;
            ReceiptNumber = receiptNumber;
            ReceiptedAmount = receiptedAmount;
            ReceiptDate = receiptDate;
            ReceiptOrder = receiptOrder;
            ReceiptStatus = receiptStatus;
            OrganizationId = organizationId;
            LegacyID = legacyID;
            DocumentId = documentId;
        }


        public Receipt(int? contributionId, int? mailingListId, int? receiptNumber, float? receiptedAmount, DateTime receiptDate, int? receiptOrder, int? receiptStatus, int? organizationId, int? legacyID, int? documentId)
        {
            ReceiptId = generateId();
            ContributionId = contributionId;
            MailingListId = mailingListId;
            ReceiptNumber = receiptNumber;
            ReceiptedAmount = receiptedAmount;
            ReceiptDate = receiptDate;
            ReceiptOrder = receiptOrder;
            ReceiptStatus = receiptStatus;
            OrganizationId = organizationId;
            LegacyID = legacyID;
            DocumentId = documentId;
        }

        public Receipt()
        {
        }

        public Result isValid()
        {
            int contador = 0;
            Receipt data = new Receipt(ReceiptId, ContributionId, MailingListId, ReceiptNumber, ReceiptedAmount,
                ReceiptDate, ReceiptOrder, ReceiptStatus, OrganizationId, LegacyID, DocumentId);
            if (validateLimitInt((int)ReceiptId) == 0 ||
                validateLimitInt((int)ContributionId) == 0 ||
                validateLimitInt((int)MailingListId) == 0 ||
                validateLimitInt((int)ReceiptNumber) == 0 ||
                validateLimitInt((int)ReceiptOrder) == 0 ||
                validateLimitInt((int)ReceiptStatus) == 0 ||
                validateLimitInt((int)OrganizationId) == 0 ||
                validateLimitInt((int)LegacyID) == 0 ||
                validateLimitInt((int)DocumentId) == 0 
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }

            if (verifyDate(ReceiptDate) == false)
            {
                contador++;
                listError.Add(new Error(contador, "Las fechas no pueden ser menores de 3 anios."));
            }


            if (ReceiptId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ReceiptId"));
            }
            if (ContributionId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionId"));
            }
            if (MailingListId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingListId"));
            }
            if (ReceiptNumber == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ReceiptNumber"));
            }
            if (ReceiptedAmount == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ReceiptedAmount"));
            }
            if (ReceiptDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ReceiptDate"));
            }
            if (ReceiptOrder == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ReceiptOrder"));
            }
            if (ReceiptStatus == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ReceiptStatus"));
            }
            if (OrganizationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta OrganizationId"));
            }
            if (LegacyID == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LegacyID"));
            }
            if (DocumentId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DocumentId"));
            }

            //last validation
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }

        public int generateId()
        {
            return contadorId;
        }

        public override string ToString()
        {
            ReceiptedAmount = convertFloat((float)ReceiptedAmount, (int)config.numeroDecimales);
            string info = "ReceiptId: " + ReceiptId + "\n" +
                            "ContributionId: " + ContributionId + "\n" +
                            "MailingListId: " + MailingListId + "\n" +
                            "ReceiptNumber: " + ReceiptNumber + "\n" +
                            "ReceiptedAmount: " + ReceiptedAmount + "\n" +
                            "ReceiptDate: " + ReceiptDate + "\n" +
                            "ReceiptOrder: " + ReceiptOrder + "\n" +
                            "ReceiptStatus: " + ReceiptStatus + "\n" +
                            "OrganizationId: " + OrganizationId + "\n" +
                            "LegacyID: " + LegacyID + "\n" +
                            "DocumentId: " + DocumentId + "\n";
            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }

        public static float convertFloat(float value, int digits)
        {
            double result = Math.Round(value, digits);
            return (float)result;
        }

        public static Boolean verifyDate(DateTime date)
        {
            TimeSpan result = today - date;
            int years = (initialTime + result).Year - 1;
            return !(years < 3);
        }
    }
}