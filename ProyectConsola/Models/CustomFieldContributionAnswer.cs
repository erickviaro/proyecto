﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class CustomFieldContributionAnswer
    {
        static Configuracion config = new Configuracion(2, 2000);
        public int? CustomFieldContributionId { get; set; }
        public int? ContributionId { get; set; }
        public int? CustomFieldAnswerId { get; set; }
        public int? LegacyId { get; set; }
        public int? DataOrgId { get; set; }
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public CustomFieldContributionAnswer(int? contributionId, 
                                                int? customFieldAnswerId, int? legacyId, int? dataOrgId)
        {
            CustomFieldContributionId = generateId();
            ContributionId = contributionId;
            CustomFieldAnswerId = customFieldAnswerId;
            LegacyId = legacyId;
            DataOrgId = dataOrgId;
        }

        public CustomFieldContributionAnswer(int? customFieldContributionId, int? contributionId,
                                        int? customFieldAnswerId, int? legacyId, int? dataOrgId)
        {
            CustomFieldContributionId = customFieldContributionId;
            ContributionId = contributionId;
            CustomFieldAnswerId = customFieldAnswerId;
            LegacyId = legacyId;
            DataOrgId = dataOrgId;
        }

        public CustomFieldContributionAnswer()
        {
        }

        public int generateId()
        {
            return contadorId;
        }

        public Result isValid()
        {
            int contador = 0;
            CustomFieldContributionAnswer data = new CustomFieldContributionAnswer();
            if (validateLimitInt((int)CustomFieldContributionId) == 0 ||
               validateLimitInt((int)ContributionId) == 0 ||
               validateLimitInt((int)CustomFieldAnswerId) == 0 ||
               validateLimitInt((int)LegacyId) == 0 ||
               validateLimitInt((int)DataOrgId) == 0 
               )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }

            if (CustomFieldContributionId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta CustomFieldContributionId"));
            }

            if (ContributionId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionId"));
            }

            if (CustomFieldAnswerId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta CustomFieldAnswerId"));
            }

            if (LegacyId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LegacyId"));
            }

            if (DataOrgId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DataOrgId"));
            }

            //last verification
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }

        public override string ToString()
        {
            string info = "CustomFieldContributionId: " + CustomFieldContributionId + "\n" +
                            "ContributionId: " + ContributionId + "\n" +
                            "CustomFieldAnswerId: " + CustomFieldAnswerId + "\n" +
                            "LegacyId: " + LegacyId + "\n" +
                            "DataOrgId: " + DataOrgId + "\n";

            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }
    }
}