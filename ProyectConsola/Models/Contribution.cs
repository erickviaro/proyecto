﻿using ProyectConsola.Models;
using Proyecto.Filters;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Proyecto.Models
{
    public class Contribution
    {
        static Configuracion config = new Configuracion(2, 200);
        public int? ContributionId { get; set; }
        public int? OrganizationId { get; set; }
        public int? ConstituentId { get; set; }
        public int? PaymentMethod { get; set; }
        public float? TotalAmount { get; set; }
        public int? ContributionType { get; set; }
        public int? UseReceipt { get; set; }
        public int? ReceiptAmount { get; set; }
        public int? EventId { get; set; }
        public int? AcknowledgementId { get; set; }
        public int? CampaignId { get; set; }
        public int? AppealId { get; set; }
        public int? IsDeleted { get; set; }
        public int? TributeId { get; set; }
        public int? DesignationId { get; set; }
        public int? IsAnonymous { get; set; }
        public int? AcknowledgementStatus { get; set; }
        public int? FeesPaidByOrg { get; set; }
        public int? FeesPaidByDonor { get; set; }
        public int? DataOrgId { get; set; }
        public int? LegacyId { get; set; }
        public int? Receipt { get; set; }
        public float? ReceiptedAmount { get; set; }
        public int? ReceiptNumber { get; set; }
        public float? DisbursementAmount { get; set; }
        public float? DonationAmountForeignCurrency { get; set; }
        public float? FeesPaidByDonorForeignCurrency { get; set; }
        public float? FeesPaidByOrgForeignCurrency { get; set; }
        public int? LegacyPID { get; set; }
        public string ContributionGuid { get; set; }
        public string Notes { get; set; }
        public string DisbursementStatus { get; set; }
        public string EntityGuid { get; set; }
        public string ExternalTransactionId { get; set; }
        public string DisbursementPaymentId { get; set; }
        public string DisbursementMethod { get; set; }
        public string TaxReceiptNumber { get; set; }
        public string TaxReceiptIssueDate { get; set; }
        public string TaxReceiptStatus { get; set; }
        public string RecurringContributionPlanGuid { get; set; }
        public DateTime InsertedDateTime { get; set; }
        public DateTime LastUpdatedDateTime { get; set; }
        public DateTime DonationDate { get; set; }
        public DateTime AcknowledgementDate { get; set; }
        public DateTime DisbursementDate { get; set; }
        public DateTime ReceiptDate { get; set; }

        static DateTime today = DateTime.Now;
        static DateTime initialTime = new DateTime(1, 1, 1);
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public Contribution(int? contributionId, int? organizationId, int? constituentId, int? paymentMethod, float? totalAmount, int? contributionType, int? useReceipt, int? receiptAmount, int? eventId, int? acknowledgementId, int? campaignId, int? appealId, int? isDeleted, int? tributeId, int? designationId, int? isAnonymous, int? acknowledgementStatus, int? feesPaidByOrg, int? feesPaidByDonor, int? dataOrgId, int? legacyId, int? receipt, float? receiptedAmount, int? receiptNumber, float? disbursementAmount, float? donationAmountForeignCurrency, float? feesPaidByDonorForeignCurrency, float? feesPaidByOrgForeignCurrency, int? legacyPID, string contributionGuid, string notes, string disbursementStatus, string entityGuid, string externalTransactionId, string disbursementPaymentId, string disbursementMethod, string taxReceiptNumber, string taxReceiptIssueDate, string taxReceiptStatus, string recurringContributionPlanGuid, DateTime donationDate, DateTime acknowledgementDate, DateTime disbursementDate, DateTime receiptDate)
        {
            ContributionId = contributionId;
            OrganizationId = organizationId;
            ConstituentId = constituentId;
            PaymentMethod = paymentMethod;
            TotalAmount = totalAmount;
            ContributionType = contributionType;
            UseReceipt = useReceipt;
            ReceiptAmount = receiptAmount;
            EventId = eventId;
            AcknowledgementId = acknowledgementId;
            CampaignId = campaignId;
            AppealId = appealId;
            IsDeleted = isDeleted;
            TributeId = tributeId;
            DesignationId = designationId;
            IsAnonymous = isAnonymous;
            AcknowledgementStatus = acknowledgementStatus;
            FeesPaidByOrg = feesPaidByOrg;
            FeesPaidByDonor = feesPaidByDonor;
            DataOrgId = dataOrgId;
            LegacyId = legacyId;
            Receipt = receipt;
            ReceiptedAmount = receiptedAmount;
            ReceiptNumber = receiptNumber;
            DisbursementAmount = disbursementAmount;
            DonationAmountForeignCurrency = donationAmountForeignCurrency;
            FeesPaidByDonorForeignCurrency = feesPaidByDonorForeignCurrency;
            FeesPaidByOrgForeignCurrency = feesPaidByOrgForeignCurrency;
            LegacyPID = legacyPID;
            ContributionGuid = contributionGuid;
            Notes = notes;
            DisbursementStatus = disbursementStatus;
            EntityGuid = entityGuid;
            ExternalTransactionId = externalTransactionId;
            DisbursementPaymentId = disbursementPaymentId;
            DisbursementMethod = disbursementMethod;
            TaxReceiptNumber = taxReceiptNumber;
            TaxReceiptIssueDate = taxReceiptIssueDate;
            TaxReceiptStatus = taxReceiptStatus;
            RecurringContributionPlanGuid = recurringContributionPlanGuid;
            InsertedDateTime = DateTime.UtcNow;
            LastUpdatedDateTime = DateTime.UtcNow;
            DonationDate = donationDate;
            AcknowledgementDate = acknowledgementDate;
            DisbursementDate = disbursementDate;
            ReceiptDate = receiptDate;
        }

        public Contribution(int? organizationId, int? constituentId, int? paymentMethod, 
            float? totalAmount, int? contributionType, int? useReceipt, int? receiptAmount, int? eventId,
            int? acknowledgementId, int? campaignId, int? appealId, int? isDeleted, int? tributeId,
            int? designationId, int? isAnonymous, int? acknowledgementStatus, int? feesPaidByOrg,
            int? feesPaidByDonor, int? dataOrgId, int? legacyId, int? receipt, float? receiptedAmount,
            int? receiptNumber, float? disbursementAmount, float? donationAmountForeignCurrency,
            float? feesPaidByDonorForeignCurrency, float? feesPaidByOrgForeignCurrency, int? legacyPID,
            string contributionGuid, string notes, string disbursementStatus, string entityGuid,
            string externalTransactionId, string disbursementPaymentId, string disbursementMethod,
            string taxReceiptNumber, string taxReceiptIssueDate, string taxReceiptStatus,
            string recurringContributionPlanGuid,
            DateTime donationDate, DateTime acknowledgementDate, DateTime disbursementDate,
            DateTime receiptDate)
        {
            ContributionId = generateId();
            OrganizationId = organizationId;
            ConstituentId = constituentId;
            PaymentMethod = paymentMethod;
            TotalAmount = totalAmount;
            ContributionType = contributionType;
            UseReceipt = useReceipt;
            ReceiptAmount = receiptAmount;
            EventId = eventId;
            AcknowledgementId = acknowledgementId;
            CampaignId = campaignId;
            AppealId = appealId;
            IsDeleted = isDeleted;
            TributeId = tributeId;
            DesignationId = designationId;
            IsAnonymous = isAnonymous;
            AcknowledgementStatus = acknowledgementStatus;
            FeesPaidByOrg = feesPaidByOrg;
            FeesPaidByDonor = feesPaidByDonor;
            DataOrgId = dataOrgId;
            LegacyId = legacyId;
            Receipt = receipt;
            ReceiptedAmount = receiptedAmount;
            ReceiptNumber = receiptNumber;
            DisbursementAmount = disbursementAmount;
            DonationAmountForeignCurrency = donationAmountForeignCurrency;
            FeesPaidByDonorForeignCurrency = feesPaidByDonorForeignCurrency;
            FeesPaidByOrgForeignCurrency = feesPaidByOrgForeignCurrency;
            LegacyPID = legacyPID;
            ContributionGuid = contributionGuid;
            Notes = notes;
            DisbursementStatus = disbursementStatus;
            EntityGuid = entityGuid;
            ExternalTransactionId = externalTransactionId;
            DisbursementPaymentId = disbursementPaymentId;
            DisbursementMethod = disbursementMethod;
            TaxReceiptNumber = taxReceiptNumber;
            TaxReceiptIssueDate = taxReceiptIssueDate;
            TaxReceiptStatus = taxReceiptStatus;
            RecurringContributionPlanGuid = recurringContributionPlanGuid;
            InsertedDateTime = DateTime.UtcNow;
            LastUpdatedDateTime = DateTime.UtcNow;
            DonationDate = donationDate;
            AcknowledgementDate = acknowledgementDate;
            DisbursementDate = disbursementDate;
            ReceiptDate = receiptDate;
        }

        public Contribution()
        {
        }

        public int generateId()
        {
            return contadorId;
        }

        public Result isValid()
        {
            Contribution data = new Contribution(ContributionId, OrganizationId, ConstituentId, 
                PaymentMethod, TotalAmount, ContributionType, UseReceipt, ReceiptAmount, EventId,
                AcknowledgementId, CampaignId, AppealId, IsDeleted, TributeId, DesignationId,
                IsAnonymous, AcknowledgementStatus, FeesPaidByOrg, FeesPaidByDonor, DataOrgId,
                LegacyId, Receipt, ReceiptedAmount, ReceiptNumber, DisbursementAmount, DonationAmountForeignCurrency,
                FeesPaidByDonorForeignCurrency, FeesPaidByOrgForeignCurrency, LegacyPID, ContributionGuid, Notes,
                DisbursementStatus, EntityGuid, ExternalTransactionId, DisbursementPaymentId, DisbursementMethod,
                TaxReceiptNumber, TaxReceiptIssueDate, TaxReceiptStatus, RecurringContributionPlanGuid,
                DonationDate, AcknowledgementDate, DisbursementDate, ReceiptDate);
            int contador = 0;
            if (validateLimitInt((int)ContributionId) == 0 ||
                validateLimitInt((int)OrganizationId) == 0 ||
                validateLimitInt((int)ConstituentId) == 0 ||
                validateLimitInt((int)PaymentMethod) == 0 ||
                validateLimitInt((int)TotalAmount) == 0 ||
                validateLimitInt((int)ContributionType) == 0 ||
                validateLimitInt((int)UseReceipt) == 0 ||
                validateLimitInt((int)ReceiptAmount) == 0 ||
                validateLimitInt((int)EventId) == 0 ||
                validateLimitInt((int)AcknowledgementId) == 0 ||
                validateLimitInt((int)CampaignId) == 0 ||
                validateLimitInt((int)AppealId) == 0 ||
                validateLimitInt((int)IsDeleted) == 0 ||
                validateLimitInt((int)TributeId) == 0 ||
                validateLimitInt((int)DesignationId) == 0 ||
                validateLimitInt((int)IsAnonymous) == 0 ||
                validateLimitInt((int)AcknowledgementStatus) == 0 ||
                validateLimitInt((int)FeesPaidByOrg) == 0 ||
                validateLimitInt((int)FeesPaidByDonor) == 0 ||
                validateLimitInt((int)DataOrgId) == 0 ||
                validateLimitInt((int)LegacyId) == 0 ||
                validateLimitInt((int)Receipt) == 0 ||
                validateLimitInt((int)ReceiptNumber) == 0 ||
                validateLimitInt((int)LegacyPID) == 0 
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de " + config.limiteNumerico));
            }

            if (verifyDate(AcknowledgementDate) == false ||
                verifyDate(DisbursementDate) == false ||
                verifyDate(DisbursementDate) == false ||
                verifyDate(ReceiptDate) == false
                )
            {
                contador++;
                listError.Add(new Error(contador, "Las fechas no pueden ser menores de 3 anios."));
            }

            if (ContributionId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionId"));
            }
            if (OrganizationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionId"));
            }
            if (ConstituentId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionId"));
            }
            if (PaymentMethod == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionId"));
            }
            if (TotalAmount == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionId"));
            }
            if (ContributionType == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionType"));
            }
            if (UseReceipt == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta UseReceipt"));
            }
            if (ReceiptAmount == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ReceiptAmount"));
            }
            if (EventId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta EventId"));
            }
            if (AcknowledgementId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AcknowledgementId"));
            }
            if (CampaignId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta CampaignId"));
            }
            if (AppealId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AppealId"));
            }
            if (IsDeleted == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsDeleted"));
            }
            if (TributeId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta TributeId"));
            }
            if (DesignationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DesignationId"));
            }
            if (IsAnonymous == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsAnonymous"));
            }
            if (AcknowledgementStatus == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AcknowledgementStatus"));
            }
            if (FeesPaidByOrg == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta FeesPaidByOrg"));
            }
            if (FeesPaidByDonor == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta FeesPaidByDonor"));
            }
            if (DataOrgId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DataOrgId"));
            }
            if (LegacyId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LegacyId"));
            }
            if (Receipt == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta Receipt"));
            }
            if (ReceiptedAmount == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ReceiptedAmount"));
            }
            if (ReceiptNumber == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ReceiptNumber"));
            }
            if (DisbursementAmount == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DisbursementAmount"));
            }
            if (DonationAmountForeignCurrency == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DonationAmountForeignCurrency"));
            }
            if (FeesPaidByDonorForeignCurrency == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta FeesPaidByDonorForeignCurrency"));
            }
            if (FeesPaidByOrgForeignCurrency == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta FeesPaidByOrgForeignCurrency"));
            }
            if (LegacyPID == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LegacyPID"));
            }
            if (ContributionGuid == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionGuid"));
            }
            if (Notes == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta Notes"));
            }
            if (DisbursementStatus == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DisbursementStatus"));
            }
            if (EntityGuid == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta EntityGuid"));
            }
            if (ExternalTransactionId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ExternalTransactionId"));
            }
            if (DisbursementPaymentId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DisbursementPaymentId"));
            }
            if (DisbursementMethod == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DisbursementMethod"));
            }
            if (TaxReceiptNumber == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta TaxReceiptNumber"));
            }
            if (TaxReceiptIssueDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta TaxReceiptIssueDate"));
            }
            if (TaxReceiptStatus == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta TaxReceiptStatus"));
            }
            if (RecurringContributionPlanGuid == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta RecurringContributionPlanGuid"));
            }
            if (InsertedDateTime == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta InsertedDateTime"));
            }
            if (LastUpdatedDateTime == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LastUpdatedDateTime"));

            }
            if (DonationDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DonationDate"));
            }
            if (AcknowledgementDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AcknowledgementDate"));
            }
            if (DisbursementDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DisbursementDate"));
            }
            if (ReceiptDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ReceiptDate"));
            }

            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }

        public static float convertFloat(float value, int digits)
        {
            double result = Math.Round(value,digits);
            return (float)result;
        }

        public override string ToString()
        {
            TotalAmount = convertFloat((float)TotalAmount, (int)config.numeroDecimales);
            ReceiptedAmount = convertFloat((float)ReceiptedAmount, (int)config.numeroDecimales);
            DisbursementAmount = convertFloat((float)ReceiptedAmount, (int)config.numeroDecimales);
            DonationAmountForeignCurrency = convertFloat((float)DonationAmountForeignCurrency, (int)config.numeroDecimales);
            FeesPaidByDonorForeignCurrency = convertFloat((float)FeesPaidByDonorForeignCurrency, (int)config.numeroDecimales);
            FeesPaidByOrgForeignCurrency = convertFloat((float)FeesPaidByOrgForeignCurrency, (int)config.numeroDecimales);

            string info = "ContributionId: " + ContributionId + "\n" +
                            "OrganizationId: " + OrganizationId + "\n" +
                            "ConstituentId: " + ConstituentId + "\n" +
                            "PaymentMethod: " + PaymentMethod + "\n" +
                            "TotalAmount: " + TotalAmount + "\n" +
                            "ContributionType: " + ContributionType + "\n" +
                            "UseReceipt: " + UseReceipt + "\n" +
                            "ReceiptAmount: " + ReceiptAmount + "\n" +
                            "EventId: " + EventId + "\n" +
                            "AcknowledgementId: " + AcknowledgementId + "\n" +
                            "CampaignId: " + CampaignId + "\n" +
                            "AppealId: " + AppealId + "\n" +
                            "IsDeleted: " + IsDeleted + "\n" +
                            "TributeId: " + TributeId + "\n" +
                            "DesignationId: " + DesignationId + "\n" +
                            "IsAnonymous: " + IsAnonymous + "\n" +
                            "AcknowledgementStatus: " + AcknowledgementStatus + "\n" +
                            "FeesPaidByOrg: " + FeesPaidByOrg + "\n" +
                            "FeesPaidByDonor: " + FeesPaidByDonor + "\n" +
                            "DataOrgId: " + DataOrgId + "\n" +
                            "LegacyId: " + LegacyId + "\n" +
                            "Receipt: " + Receipt + "\n" +
                            "ReceiptedAmount: " + ReceiptedAmount + "\n" +
                            "ReceiptNumber: " + ReceiptNumber + "\n" +
                            "DisbursementAmount: " + DisbursementAmount + "\n" +
                            "DonationAmountForeignCurrency: " + DonationAmountForeignCurrency + "\n" +
                            "FeesPaidByDonorForeignCurrency: " + FeesPaidByDonorForeignCurrency + "\n" +
                            "FeesPaidByOrgForeignCurrency: " + FeesPaidByOrgForeignCurrency + "\n" +
                            "LegacyPID: " + LegacyPID + "\n" +
                            "ContributionGuid: " + ContributionGuid + "\n" +
                            "Notes: " + Notes + "\n" +
                            "DisbursementStatus: " + DisbursementStatus + "\n" +
                            "EntityGuid: " + EntityGuid + "\n" +
                            "ExternalTransactionId: " + ExternalTransactionId + "\n" +
                            "DisbursementPaymentId: " + DisbursementPaymentId + "\n" +
                            "TaxReceiptNumber: " + TaxReceiptNumber + "\n" +
                            "TaxReceiptIssueDate: " + TaxReceiptIssueDate + "\n" +
                            "TaxReceiptStatus: " + TaxReceiptStatus + "\n" +
                            "RecurringContributionPlanGuid: " + RecurringContributionPlanGuid + "\n" +
                            "InsertedDateTime: " + InsertedDateTime + "\n" +
                            "LastUpdatedDateTime: " + LastUpdatedDateTime + "\n" +
                            "DonationDate: " + DonationDate + "\n" +
                            "AcknowledgementDate: " + AcknowledgementDate + "\n" +
                            "DisbursementDate: " + DisbursementDate + "\n" +
                            "ReceiptDate: " + ReceiptDate + "\n";
            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }

        public static Boolean verifyDate(DateTime date)
        {
            TimeSpan result = today - date;
            int years = (initialTime + result).Year - 1;
            return !(years < 3);
        }
    }
}