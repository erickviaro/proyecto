﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class Event
    {
        static Configuracion config = new Configuracion(2, 2000);
        public int? EventId { get; set; }
        public int? OrganizationId { get; set; }
        public int? EventStatusId { get; set; }
        public int? CategoryId { get; set; }
        public int? MaxCapacity { get; set; }
        public int? MinCapacity { get; set; }
        public float? IncomeGoal { get; set; }
        public int? AttendanceGoal { get; set; }
        public int? CampaignId { get; set; }
        public int? AppealId { get; set; }
        public int? FundId { get; set; }
        public int? IsDeleted { get; set; }
        public int? EVENT_ID { get; set; }
        public int? LegID { get; set; }
        public int? DataOrgId { get; set; }
        public int? LegacyID { get; set; }
        public int? EventCurrencyId { get; set; }
        public int? EventType { get; set; }
        public int? IsDefaultEvent { get; set; }
        public int? ProjectTypeId { get; set; }

        public string EventGuid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
        public string VenueName { get; set; }
        public string TimeZone { get; set; }
        public string EventUrl { get; set; }

        public DateTime InsertedDateTime { get; set; }
        public DateTime LastUpdatedDateTime { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public DateTime RSVPDateTime { get; set; }
        public DateTime FundraisingEndDate { get; set; }
        public DateTime LiveEventOpen { get; set; }
        public DateTime LiveEventClose { get; set; }
        public DateTime OnlineEventOpen { get; set; }
        public DateTime OnlineEventClose { get; set; }

        static DateTime today = DateTime.Now;
        static DateTime initialTime = new DateTime(1, 1, 1);
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public Event(int? eventId, int? organizationId, int? eventStatusId, int? categoryId, int? maxCapacity, int? minCapacity, float? incomeGoal, int? attendanceGoal, int? campaignId, int? appealId, int? fundId, int? isDeleted, int? eVENT_ID, int? legID, int? dataOrgId, int? legacyID, int? eventCurrencyId, int? eventType, int? isDefaultEvent, int? projectTypeId, string eventGuid, string name, string description, string notes, string venueName, string timeZone, string eventUrl, DateTime startDateTime, DateTime endDateTime, DateTime rSVPDateTime, DateTime fundraisingEndDate, DateTime liveEventOpen, DateTime liveEventClose, DateTime onlineEventOpen, DateTime onlineEventClose)
        {
            EventId = eventId;
            OrganizationId = organizationId;
            EventStatusId = eventStatusId;
            CategoryId = categoryId;
            MaxCapacity = maxCapacity;
            MinCapacity = minCapacity;
            IncomeGoal = incomeGoal;
            AttendanceGoal = attendanceGoal;
            CampaignId = campaignId;
            AppealId = appealId;
            FundId = fundId;
            IsDeleted = isDeleted;
            EVENT_ID = eVENT_ID;
            LegID = legID;
            DataOrgId = dataOrgId;
            LegacyID = legacyID;
            EventCurrencyId = eventCurrencyId;
            EventType = eventType;
            IsDefaultEvent = isDefaultEvent;
            ProjectTypeId = projectTypeId;
            EventGuid = eventGuid;
            Name = name;
            Description = description;
            Notes = notes;
            VenueName = venueName;
            TimeZone = timeZone;
            EventUrl = eventUrl;
            InsertedDateTime = DateTime.UtcNow;
            LastUpdatedDateTime = DateTime.UtcNow;
            StartDateTime = startDateTime;
            EndDateTime = endDateTime;
            RSVPDateTime = rSVPDateTime;
            FundraisingEndDate = fundraisingEndDate;
            LiveEventOpen = liveEventOpen;
            LiveEventClose = liveEventClose;
            OnlineEventOpen = onlineEventOpen;
            OnlineEventClose = onlineEventClose;
        }

        public Event(int? organizationId, int? eventStatusId, int? categoryId, int? maxCapacity, int? minCapacity, float? incomeGoal, int? attendanceGoal, int? campaignId, int? appealId, int? fundId, int? isDeleted, int? eVENT_ID, int? legID, int? dataOrgId, int? legacyID, int? eventCurrencyId, int? eventType, int? isDefaultEvent, int? projectTypeId, string eventGuid, string name, string description, string notes, string venueName, string timeZone, string eventUrl,DateTime startDateTime, DateTime endDateTime, DateTime rSVPDateTime, DateTime fundraisingEndDate, DateTime liveEventOpen, DateTime liveEventClose, DateTime onlineEventOpen, DateTime onlineEventClose)
        {
            EventId = generateId();
            OrganizationId = organizationId;
            EventStatusId = eventStatusId;
            CategoryId = categoryId;
            MaxCapacity = maxCapacity;
            MinCapacity = minCapacity;
            IncomeGoal = incomeGoal;
            AttendanceGoal = attendanceGoal;
            CampaignId = campaignId;
            AppealId = appealId;
            FundId = fundId;
            IsDeleted = isDeleted;
            EVENT_ID = eVENT_ID;
            LegID = legID;
            DataOrgId = dataOrgId;
            LegacyID = legacyID;
            EventCurrencyId = eventCurrencyId;
            EventType = eventType;
            IsDefaultEvent = isDefaultEvent;
            ProjectTypeId = projectTypeId;
            EventGuid = eventGuid;
            Name = name;
            Description = description;
            Notes = notes;
            VenueName = venueName;
            TimeZone = timeZone;
            EventUrl = eventUrl;
            InsertedDateTime = DateTime.UtcNow;
            LastUpdatedDateTime = DateTime.UtcNow;
            StartDateTime = startDateTime;
            EndDateTime = endDateTime;
            RSVPDateTime = rSVPDateTime;
            FundraisingEndDate = fundraisingEndDate;
            LiveEventOpen = liveEventOpen;
            LiveEventClose = liveEventClose;
            OnlineEventOpen = onlineEventOpen;
            OnlineEventClose = onlineEventClose;
        }

        public Event()
        {
        }

        public int generateId()
        {
            return contadorId;
        }
        public Result isValid()
        {
            int contador = 0;
            Event data = new Event(EventId, OrganizationId, EventStatusId,CategoryId,MaxCapacity,MinCapacity,IncomeGoal,
                AttendanceGoal,CampaignId,AppealId,FundId,IsDeleted,EVENT_ID,LegID,DataOrgId,LegacyID,EventCurrencyId,
                EventType,IsDefaultEvent,ProjectTypeId,EventGuid,Name,Description,Notes,VenueName,TimeZone,EventUrl,
                StartDateTime,EndDateTime,RSVPDateTime,FundraisingEndDate,LiveEventOpen,
                LiveEventClose,OnlineEventOpen,OnlineEventClose);

            if (validateLimitInt((int)EventId) == 0 ||
                validateLimitInt((int)OrganizationId) == 0 ||
                validateLimitInt((int)EventStatusId) == 0 ||
                validateLimitInt((int)CategoryId) == 0 ||
                validateLimitInt((int)MaxCapacity) == 0 ||
                validateLimitInt((int)MinCapacity) == 0 ||
                validateLimitInt((int)IncomeGoal) == 0 ||
                validateLimitInt((int)CampaignId) == 0 ||
                validateLimitInt((int)AppealId) == 0 ||
                validateLimitInt((int)FundId) == 0 ||
                validateLimitInt((int)IsDeleted) == 0 ||
                validateLimitInt((int)EVENT_ID) == 0 ||
                validateLimitInt((int)LegID) == 0 ||
                validateLimitInt((int)DataOrgId) == 0 ||
                validateLimitInt((int)LegacyID) == 0 ||
                validateLimitInt((int)EventCurrencyId) == 0 ||
                validateLimitInt((int)EventType) == 0 ||
                validateLimitInt((int)IsDefaultEvent) == 0 ||
                validateLimitInt((int)ProjectTypeId) == 0
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }

            if (verifyDate(StartDateTime) == false ||
                verifyDate(EndDateTime) == false ||
                verifyDate(RSVPDateTime) == false ||
                verifyDate(FundraisingEndDate) == false ||
                verifyDate(LiveEventOpen) == false ||
                verifyDate(LiveEventClose) == false ||
                verifyDate(OnlineEventOpen) == false ||
                verifyDate(OnlineEventClose) == false
                )
            {
                contador++;
                listError.Add(new Error(contador, "Las fechas no pueden ser menores de 3 anios."));
            }

            if (EventId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta EventId"));
            }

            if (OrganizationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta OrganizationId"));
            }

            if (EventStatusId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta EventStatusId"));
            }

            if (CategoryId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta CategoryId"));
            }

            if (MaxCapacity == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta MaxCapacity"));
            }

            if (MinCapacity == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta MinCapacity"));
            }

            if (AttendanceGoal == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ProfileTypeId"));
            }

            if (CampaignId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta CampaignId"));
            }

            if (AppealId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AppealId"));
            }

            if (FundId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta FundId"));
            }

            if (IsDeleted == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsDeleted"));
            }

            if (EVENT_ID == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta EVENT_ID"));
            }

            if (LegID == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LegID"));
            }

            if (DataOrgId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DataOrgId"));
            }

            if (LegacyID == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LegacyID"));
            }

            if (EventCurrencyId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta EventCurrencyId"));
            }

            if (EventType == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta EventType"));
            }
            if (IsDefaultEvent == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsDefaultEvent"));
            }

            if (ProjectTypeId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ProjectTypeId"));
            }
            
            if (string.IsNullOrWhiteSpace(EventGuid))
            {
                contador++;
                listError.Add(new Error(contador, "Falta EventGuid"));
            }
            if (string.IsNullOrWhiteSpace(Name))
            {
                contador++;
                listError.Add(new Error(contador, "Falta Name"));
            }
            else
            {
                if (Name.Length <= 3)
                {
                    contador++;
                    listError.Add(new Error(contador, "El nombre no puede ser menor a 3 letras"));
                }
            }
            if (string.IsNullOrWhiteSpace(Description))
            {
                contador++;
                listError.Add(new Error(contador, "Falta Description"));
            }

            if (string.IsNullOrWhiteSpace(Notes))
            {
                contador++;
                listError.Add(new Error(contador, "Falta Notes"));
            }

            if (string.IsNullOrWhiteSpace(VenueName))
            {
                contador++;
                listError.Add(new Error(contador, "Falta VenueName"));
            }
            
            if (string.IsNullOrWhiteSpace(TimeZone))
            {
                contador++;
                listError.Add(new Error(contador, "Falta TimeZone"));
            }
            if (string.IsNullOrWhiteSpace(EventUrl))
            {
                contador++;
                listError.Add(new Error(contador, "Falta EventUrl"));
            }
            
            if (InsertedDateTime == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta InsertedDateTime"));
            }
            if (LastUpdatedDateTime == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LastUpdatedDateTime"));
            }
            if (StartDateTime == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta StartDateTime"));
            }
            if (EndDateTime == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta EndDateTime"));
            }
            if (RSVPDateTime == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta RSVPDateTime"));
            }
            if (FundraisingEndDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta FundraisingEndDate"));
            }

            if (LiveEventOpen == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LiveEventOpen"));
            }
            if (LiveEventClose == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LiveEventClose"));
            }
            if (OnlineEventOpen == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta OnlineEventOpen"));
            }
            if (OnlineEventClose == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta OnlineEventClose"));
            }

            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }
        public override string ToString()
        {
            IncomeGoal = convertFloat((float)IncomeGoal, (int)config.numeroDecimales);

            string info = "EventId: " + EventId + "\n" +
                "OrganizationId: " + OrganizationId + "\n" +
                "EventStatusId: " + EventStatusId + "\n" +
                "CategoryId: " + CategoryId + "\n" +
                "MaxCapacity: " + MaxCapacity + "\n" +
                "MinCapacity: " + MinCapacity + "\n" +
                "IncomeGoal: " + IncomeGoal + "\n" +
                "AttendanceGoal: " + AttendanceGoal + "\n" +
                "CampaignId: " + CampaignId + "\n" +
                "AppealId: " + AppealId + "\n" +
                "FundId: " + FundId + "\n" +
                "IsDeleted: " + IsDeleted + "\n" +
                "EVENT_ID: " + EVENT_ID + "\n" +
                "LegID: " + LegID + "\n" +
                "DataOrgId: " + DataOrgId + "\n" +
                "LegacyID: " + LegacyID + "\n" +
                "EventCurrencyId: " + EventCurrencyId + "\n" +
                "EventType: " + EventType + "\n" +
                "IsDefaultEvent: " + IsDefaultEvent + "\n" +
                "ProjectTypeId: " + ProjectTypeId + "\n" +
                "EventGuid: " + EventGuid + "\n" +
                "Name: " + Name + "\n" +
                "Description: " + Description + "\n" +
                "Notes: " + Notes + "\n" +
                "VenueName: " + VenueName + "\n" +
                "TimeZone: " + TimeZone + "\n" +
                "EventUrl: " + EventUrl + "\n" +
                "InsertedDateTime: " + InsertedDateTime + "\n" +
                "LastUpdatedDateTime: " + LastUpdatedDateTime + "\n" +
                "StartDateTime: " + StartDateTime + "\n" +
                "EndDateTime: " + EndDateTime + "\n" +
                "RSVPDateTime: " + RSVPDateTime + "\n" +
                "FundraisingEndDate: " + FundraisingEndDate + "\n" +
                "LiveEventOpen: " + LiveEventOpen + "\n" +
                "LiveEventClose: " + LiveEventClose + "\n" +
                "OnlineEventOpen: " + OnlineEventOpen + "\n" +
                "OnlineEventClose: " + OnlineEventClose + "\n";

            return info;
        }

        public static Boolean verifyDate(DateTime date)
        {
            TimeSpan result = today - date;
            int years = (initialTime + result).Year - 1;
            if (years < 3)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }

        public static float convertFloat(float value, int digits)
        {
            double result = Math.Round(value, digits);
            return (float)result;
        }
    }
}