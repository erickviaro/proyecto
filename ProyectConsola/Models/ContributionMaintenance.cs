﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class ContributionMaintenance
    {
        static Configuracion config = new Configuracion(2, 2000);
        public int? ContributionMaintenanceId { get; set; }
        public int? ContributionId { get; set; }
        public int? MaintenanceId { get; set; }
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public ContributionMaintenance(int? contributionId, int? maintenanceId)
        {
            ContributionMaintenanceId = generateId();
            ContributionId = contributionId;
            MaintenanceId = maintenanceId;
        }

        public ContributionMaintenance(int? contributionMaintenanceId, int? contributionId, int? maintenanceId) 
        {
            ContributionMaintenanceId = contributionMaintenanceId;
            ContributionId = contributionId;
            MaintenanceId = maintenanceId;
        }

        public ContributionMaintenance()
        {
        }

        public int generateId()
        {
            return contadorId;
        }

        public Result isValid()
        {
            int contador = 0;
            ContributionMaintenance data = new ContributionMaintenance();

            if (validateLimitInt((int)ContributionMaintenanceId) == 0 ||
                validateLimitInt((int)ContributionId) == 0 ||
                validateLimitInt((int)MaintenanceId) == 0
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }
            if (ContributionMaintenanceId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionMaintenanceId"));
            }
            
            if (ContributionId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionId"));
            }
           
            if (MaintenanceId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta MaintenanceId"));
            }

            //last verification
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }

        public override string ToString()
        {
            string info = "ContributionMaintenanceId: " + ContributionMaintenanceId + "\n" +
                            "ContributionId: " + ContributionId + "\n" +
                            "MaintenanceId: " + MaintenanceId + "\n";
            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }
    }
}