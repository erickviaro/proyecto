﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class PaymentMethod
    {
        static Configuracion config = new Configuracion(2, 200);
        public int? PaymentMethodId { get; set; }
        public int? PaymentMethodTypeId { get; set; }
        public int? CreditCardId { get; set; }
        public int? LegacyID { get; set; }
        public int? DataOrgID { get; set; }
        public int? Quantity { get; set; }
        public int? IsLiquidated { get; set; }
        public int? NumberOfShares { get; set; }

        public string CheckNumber { get; set; }
        public string Description { get; set; }
        public string TransactionId { get; set; }
        public string Unit { get; set; }
        public string Securities { get; set; }
        public string ProcessorName { get; set; }
        public string BankName { get; set; }
        public string BankID { get; set; }
        public string AccountNumber { get; set; }
        public string TransitNumber { get; set; }

        public DateTime CheckDateTime { get; set; }

        static DateTime today = DateTime.Now;
        static DateTime initialTime = new DateTime(1, 1, 1);
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public PaymentMethod(int? paymentMethodId, int? paymentMethodTypeId, int? creditCardId, int? legacyID, 
                                int? dataOrgID, int? quantity, int? isLiquidated, int? numberOfShares, 
                                string checkNumber, string description, string transactionId, string unit,
                                string securities, string processorName, string bankName, string bankID, 
                                string accountNumber, string transitNumber, DateTime checkDateTime)
        {
            PaymentMethodId = paymentMethodId;
            PaymentMethodTypeId = paymentMethodTypeId;
            CreditCardId = creditCardId;
            LegacyID = legacyID;
            DataOrgID = dataOrgID;
            Quantity = quantity;
            IsLiquidated = isLiquidated;
            NumberOfShares = numberOfShares;
            CheckNumber = checkNumber;
            Description = description;
            TransactionId = transactionId;
            Unit = unit;
            Securities = securities;
            ProcessorName = processorName;
            BankName = bankName;
            BankID = bankID;
            AccountNumber = accountNumber;
            TransitNumber = transitNumber;
            CheckDateTime = checkDateTime;
        }

        public PaymentMethod(int? paymentMethodTypeId, int? creditCardId, int? legacyID,
                        int? dataOrgID, int? quantity, int? isLiquidated, int? numberOfShares,
                        string checkNumber, string description, string transactionId, string unit,
                        string securities, string processorName, string bankName, string bankID,
                        string accountNumber, string transitNumber, DateTime checkDateTime)
        {
            PaymentMethodId = generateId();
            PaymentMethodTypeId = paymentMethodTypeId;
            CreditCardId = creditCardId;
            LegacyID = legacyID;
            DataOrgID = dataOrgID;
            Quantity = quantity;
            IsLiquidated = isLiquidated;
            NumberOfShares = numberOfShares;
            CheckNumber = checkNumber;
            Description = description;
            TransactionId = transactionId;
            Unit = unit;
            Securities = securities;
            ProcessorName = processorName;
            BankName = bankName;
            BankID = bankID;
            AccountNumber = accountNumber;
            TransitNumber = transitNumber;
            CheckDateTime = checkDateTime;
        }

        public PaymentMethod()
        {
        }

        public Result isValid()
        {
            int contador = 0;
            PaymentMethod data = new PaymentMethod(PaymentMethodTypeId, CreditCardId, LegacyID,
                DataOrgID, Quantity, IsLiquidated, NumberOfShares, CheckNumber, Description, TransactionId,
                Unit, Securities, ProcessorName, BankName, BankID, AccountNumber, TransitNumber, CheckDateTime);
            if (validateLimitInt((int)PaymentMethodId) == 0 ||
                validateLimitInt((int)PaymentMethodTypeId) == 0 ||
                validateLimitInt((int)CreditCardId) == 0 ||
                validateLimitInt((int)LegacyID) == 0 ||
                validateLimitInt((int)DataOrgID) == 0 ||
                validateLimitInt((int)Quantity) == 0 ||
                validateLimitInt((int)IsLiquidated) == 0 ||
                validateLimitInt((int)NumberOfShares) == 0 
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }

            if (verifyDate(CheckDateTime) == false)
            {
                contador++;
                listError.Add(new Error(contador, "Las fechas no pueden ser menores de 3 anios."));
            }


            if (PaymentMethodId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta PaymentMethodId"));
            }
            if (PaymentMethodTypeId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta PaymentMethodTypeId"));
            }
            if (CreditCardId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta CreditCardId"));
            }
            if (LegacyID == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LegacyID"));
            }
            if (DataOrgID == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DataOrgID"));
            }
            if (Quantity == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta Quantity"));
            }
            if (IsLiquidated == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsLiquidated"));
            }
            if (NumberOfShares == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ProcesNumberOfSharessDate"));
            }
            if (CheckDateTime == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta CheckDateTime"));
            }
            if (string.IsNullOrWhiteSpace(CheckNumber))
            {
                contador++;
                listError.Add(new Error(contador, "Falta ConstituentGuid"));
            }
            if (string.IsNullOrWhiteSpace(Description))
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingName"));
            }
            if (string.IsNullOrWhiteSpace(TransactionId))
            {
                contador++;
                listError.Add(new Error(contador, "Falta UnsubscribeToken"));
            }
            if (string.IsNullOrWhiteSpace(Unit))
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingProperties"));
            }
            if (string.IsNullOrWhiteSpace(Securities))
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingProperties"));
            }
            if (string.IsNullOrWhiteSpace(ProcessorName))
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingProperties"));
            }
            if (string.IsNullOrWhiteSpace(BankName))
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingProperties"));
            }
            if (string.IsNullOrWhiteSpace(BankID))
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingProperties"));
            }
            if (string.IsNullOrWhiteSpace(AccountNumber))
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingProperties"));
            }
            if (string.IsNullOrWhiteSpace(TransitNumber))
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingProperties"));
            }

            //last validation
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }

        public int generateId()
        {
            return contadorId;
        }

        public override string ToString()
        {
            string info = "PaymentMethodId: " + PaymentMethodId + "\n" +
                            "PaymentMethodTypeId: " + PaymentMethodTypeId + "\n" +
                            "CreditCardId: " + CreditCardId + "\n" +
                            "LegacyID: " + LegacyID + "\n" +
                            "DataOrgID: " + DataOrgID + "\n" +
                            "Quantity: " + Quantity + "\n" +
                            "IsLiquidated: " + IsLiquidated + "\n" +
                            "NumberOfShares: " + NumberOfShares + "\n" +
                            "CheckNumber: " + CheckNumber + "\n" +
                            "Description: " + Description + "\n" +
                            "TransactionId: " + TransactionId + "\n" +
                            "Unit: " + Unit + "\n" +
                            "Securities: " + Securities + "\n" +
                            "ProcessorName: " + ProcessorName + "\n" +
                            "BankName: " + BankName + "\n" +
                            "BankID: " + BankID + "\n" +
                            "AccountNumber: " + AccountNumber + "\n" +
                            "TransitNumber: " + TransitNumber + "\n" +
                            "CheckDateTime: " + CheckDateTime + "\n";

            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }

        public static float convertFloat(float value, int digits)
        {
            double result = Math.Round(value, digits);
            return (float)result;
        }

        public static Boolean verifyDate(DateTime date)
        {
            TimeSpan result = today - date;
            int years = (initialTime + result).Year - 1;
            return !(years < 3);
        }
    }
}