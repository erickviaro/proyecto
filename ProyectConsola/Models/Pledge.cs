﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class Pledge
    {
        static Configuracion config = new Configuracion(2, 200);
        public int? PledgeId { get; set; }
        public int? OrganizationId { get; set; }
        public int? ConstituentId { get; set; }
        public int? TotalAmount { get; set; }
        public int? ExpectedNumberOfPayments { get; set; }
        public int? EventId { get; set; }
        public int? AcknowledgementId { get; set; }
        public int? PledgeFrequencyId { get; set; }
        public int? PledgeStatusId { get; set; }
        public int? IsDeleted { get; set; }
        public int? TributeId { get; set; }
        public int? DesignationId { get; set; }
        public int? CampaignId { get; set; }
        public int? IsAnonymous { get; set; }
        public int? FundId { get; set; }
        public int? AppealId { get; set; }
        public int? AcknowledgementStatus { get; set; }
        public int? SolicitorId { get; set; }
        public int? LegacyID { get; set; }
        public int? DataOrgId { get; set; }
        public int? MatchContributionId { get; set; }
        public string PledgeGuid { get; set; }
        public string Notes { get; set; }
        public DateTime PledgeDate { get; set; }
        public DateTime FirstPaymentDate { get; set; }
        public DateTime InsertedDateTime { get; set; }
        public DateTime LastUpdatedDateTime { get; set; }
        public DateTime AcknowledgementDate { get; set; }

        static DateTime today = DateTime.Now;
        static DateTime initialTime = new DateTime(1, 1, 1);
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public Pledge(int? pledgeId, int? organizationId, int? constituentId, int? totalAmount, 
                        int? expectedNumberOfPayments, int? eventId, int? acknowledgementId, 
                        int? pledgeFrequencyId, int? pledgeStatusId, int? isDeleted, int? tributeId, 
                        int? designationId, int? campaignId, int? isAnonymous, int? fundId, int? appealId,
                        int? acknowledgementStatus, int? solicitorId, int? legacyID, int? dataOrgId,
                        int? matchContributionId, string pledgeGuid, string notes, DateTime pledgeDate,
                        DateTime firstPaymentDate, DateTime acknowledgementDate)
        {
            PledgeId = pledgeId;
            OrganizationId = organizationId;
            ConstituentId = constituentId;
            TotalAmount = totalAmount;
            ExpectedNumberOfPayments = expectedNumberOfPayments;
            EventId = eventId;
            AcknowledgementId = acknowledgementId;
            PledgeFrequencyId = pledgeFrequencyId;
            PledgeStatusId = pledgeStatusId;
            IsDeleted = isDeleted;
            TributeId = tributeId;
            DesignationId = designationId;
            CampaignId = campaignId;
            IsAnonymous = isAnonymous;
            FundId = fundId;
            AppealId = appealId;
            AcknowledgementStatus = acknowledgementStatus;
            SolicitorId = solicitorId;
            LegacyID = legacyID;
            DataOrgId = dataOrgId;
            MatchContributionId = matchContributionId;
            PledgeGuid = pledgeGuid;
            Notes = notes;
            PledgeDate = pledgeDate;
            FirstPaymentDate = firstPaymentDate;
            InsertedDateTime = DateTime.UtcNow.Date;
            LastUpdatedDateTime = DateTime.UtcNow.Date;
            AcknowledgementDate = acknowledgementDate;
        }

        public Pledge(int? organizationId, int? constituentId, int? totalAmount,
                int? expectedNumberOfPayments, int? eventId, int? acknowledgementId,
                int? pledgeFrequencyId, int? pledgeStatusId, int? isDeleted, int? tributeId,
                int? designationId, int? campaignId, int? isAnonymous, int? fundId, int? appealId,
                int? acknowledgementStatus, int? solicitorId, int? legacyID, int? dataOrgId,
                int? matchContributionId, string pledgeGuid, string notes, DateTime pledgeDate,
                DateTime firstPaymentDate, DateTime acknowledgementDate)
        {
            PledgeId = generateId();
            OrganizationId = organizationId;
            ConstituentId = constituentId;
            TotalAmount = totalAmount;
            ExpectedNumberOfPayments = expectedNumberOfPayments;
            EventId = eventId;
            AcknowledgementId = acknowledgementId;
            PledgeFrequencyId = pledgeFrequencyId;
            PledgeStatusId = pledgeStatusId;
            IsDeleted = isDeleted;
            TributeId = tributeId;
            DesignationId = designationId;
            CampaignId = campaignId;
            IsAnonymous = isAnonymous;
            FundId = fundId;
            AppealId = appealId;
            AcknowledgementStatus = acknowledgementStatus;
            SolicitorId = solicitorId;
            LegacyID = legacyID;
            DataOrgId = dataOrgId;
            MatchContributionId = matchContributionId;
            PledgeGuid = pledgeGuid;
            Notes = notes;
            PledgeDate = pledgeDate;
            FirstPaymentDate = firstPaymentDate;
            InsertedDateTime = DateTime.UtcNow.Date;
            LastUpdatedDateTime = DateTime.UtcNow.Date;
            AcknowledgementDate = acknowledgementDate;
        }

        public Pledge()
        {
        }

        public Result isValid()
        {
            int contador = 0;
            Pledge data = new Pledge(PledgeId, OrganizationId, ConstituentId, TotalAmount, ExpectedNumberOfPayments,
                EventId, AcknowledgementId, PledgeFrequencyId, PledgeStatusId, IsDeleted, TributeId, DesignationId,
                CampaignId, IsAnonymous, FundId, AppealId, AcknowledgementStatus, SolicitorId, LegacyID, DataOrgId,
                MatchContributionId, PledgeGuid, Notes, PledgeDate, FirstPaymentDate, AcknowledgementDate);
            if (validateLimitInt((int)PledgeId) == 0 ||
                validateLimitInt((int)OrganizationId) == 0 ||
                validateLimitInt((int)ConstituentId) == 0 ||
                validateLimitInt((int)TotalAmount) == 0 ||
                validateLimitInt((int)ExpectedNumberOfPayments) == 0 ||
                validateLimitInt((int)EventId) == 0 ||
                validateLimitInt((int)AcknowledgementId) == 0 ||
                validateLimitInt((int)PledgeFrequencyId) == 0 ||
                validateLimitInt((int)PledgeStatusId) == 0 ||
                validateLimitInt((int)IsDeleted) == 0 ||
                validateLimitInt((int)TributeId) == 0 ||
                validateLimitInt((int)DesignationId) == 0 ||
                validateLimitInt((int)CampaignId) == 0 ||
                validateLimitInt((int)IsAnonymous) == 0 ||
                validateLimitInt((int)FundId) == 0 ||
                validateLimitInt((int)AppealId) == 0 ||
                validateLimitInt((int)AcknowledgementStatus) == 0 ||
                validateLimitInt((int)SolicitorId) == 0 ||
                validateLimitInt((int)LegacyID) == 0 ||
                validateLimitInt((int)DataOrgId) == 0 ||
                validateLimitInt((int)MatchContributionId) == 0
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }

            if (verifyDate(PledgeDate) == false ||
                verifyDate(FirstPaymentDate) == false ||
                verifyDate(AcknowledgementDate) == false
                )
            {
                contador++;
                listError.Add(new Error(contador, "Las fechas no pueden ser menores de 3 anios."));
            }

            if (PledgeId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta PledgeId"));
            }
            if (OrganizationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta OrganizationId"));
            }
            if (ConstituentId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ConstituentId"));
            }
            if (TotalAmount == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta TotalAmount"));
            }
            if (ExpectedNumberOfPayments == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ExpectedNumberOfPayments"));
            }
            if (EventId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta EventId"));
            }
            if (AcknowledgementId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AcknowledgementId"));
            }
            if (PledgeFrequencyId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta PledgeFrequencyId"));
            }
            if (PledgeStatusId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta PledgeStatusId"));
            }
            if (IsDeleted == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsDeleted"));
            }
            if (TributeId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta TributeId"));
            }
            if (DesignationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta PledgeStatusId"));
            }
            if (CampaignId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta CampaignId"));
            }
            if (IsAnonymous == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsAnonymous"));
            }
            if (FundId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta FundId"));
            }
            if (AppealId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AppealId"));
            }
            if (AcknowledgementStatus == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AcknowledgementStatus"));
            }
            if (SolicitorId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta SolicitorId"));
            }
            if (LegacyID == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LegacyID"));
            }
            if (DataOrgId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DataOrgId"));
            }
            if (MatchContributionId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta MatchContributionId"));
            }
            if (PledgeDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta PledgeDate"));
            }
            if (FirstPaymentDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta FirstPaymentDate"));
            }
            if (InsertedDateTime == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta InsertedDateTime"));
            }
            if (LastUpdatedDateTime == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LastUpdatedDateTime"));
            }
            if (AcknowledgementDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AcknowledgementDate"));
            }
            if (string.IsNullOrWhiteSpace(PledgeGuid))
            {
                contador++;
                listError.Add(new Error(contador, "Falta ConstituentGuid"));
            }
            if (string.IsNullOrWhiteSpace(Notes))
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingName"));
            }

            //last validation
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }

        public int generateId()
        {
            return contadorId;
        }

        public override string ToString()
        {
            string info = "PledgeId: " + PledgeId + "\n" +
                            "OrganizationId: " + OrganizationId + "\n" +
                            "ConstituentId: " + ConstituentId + "\n" +
                            "TotalAmount: " + TotalAmount + "\n" +
                            "ExpectedNumberOfPayments: " + ExpectedNumberOfPayments + "\n" +
                            "EventId: " + EventId + "\n" +
                            "AcknowledgementId: " + AcknowledgementId + "\n" +
                            "PledgeFrequencyId: " + PledgeFrequencyId + "\n" +
                            "PledgeStatusId: " + PledgeStatusId + "\n" +
                            "IsDeleted: " + IsDeleted + "\n" +
                            "TributeId: " + TributeId + "\n" +
                            "DesignationId: " + DesignationId + "\n" +
                            "CampaignId: " + CampaignId + "\n" +
                            "IsAnonymous: " + IsAnonymous + "\n" +
                            "FundId: " + FundId + "\n" +
                            "AppealId: " + AppealId + "\n" +
                            "AcknowledgementStatus: " + AcknowledgementStatus + "\n" +
                            "SolicitorId: " + SolicitorId + "\n" +
                            "LegacyID: " + LegacyID + "\n" +
                            "DataOrgId: " + DataOrgId + "\n" +
                            "MatchContributionId: " + MatchContributionId + "\n" +
                            "PledgeGuid: " + PledgeGuid + "\n" +
                            "Notes: " + Notes + "\n" +
                            "PledgeDate: " + PledgeDate + "\n" +
                            "FirstPaymentDate: " + FirstPaymentDate + "\n" +
                            "InsertedDateTime: " + InsertedDateTime + "\n" +
                            "LastUpdatedDateTime: " + LastUpdatedDateTime + "\n" +
                            "AcknowledgementDate: " + AcknowledgementDate + "\n";

            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }

        public static float convertFloat(float value, int digits)
        {
            double result = Math.Round(value, digits);
            return (float)result;
        }

        public static Boolean verifyDate(DateTime date)
        {
            TimeSpan result = today - date;
            int years = (initialTime + result).Year - 1;
            return !(years < 3);
        }
    }
}