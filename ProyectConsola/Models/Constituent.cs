﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Proyecto.Models
{
    public class Constituent
    {
        static Configuracion config = new Configuracion(2,2000);
        public int? ConstituentId { get; set; }
        public int? OrganizationId { get; set; }
        public int? StatusId { get; set; }
        public int? VolunteerProfileId { get; set; }
        public int? CommunicationPermission { get; set; }
        public int? BackgoundStatus { get; set; }
        public int? PreferredMOC { get; set; }
        public int? ProfileTypeId { get; set; }
        public int? ImageId { get; set; }
        public int? IsAffiliate { get; set; }
        public int? SmallImageId { get; set; }
        public int? LegID { get; set; }
        public int? IsDeleted { get; set; }
        public int? Gender { get; set; }
        public int? MaritalStatus { get; set; }
        public int? EducationLevel { get; set; }
        public int? EmploymentStatus { get; set; }
        public int? JobTitleId { get; set; }
        public int? ContactTypeId { get; set; }
        public int? DataOrgId { get; set; }
        public int? LegacyId { get; set; }
        public int? SourceId { get; set; }
        public int? PrefixId { get; set; }
        public int? ParentConstituentId { get; set; }
        public int? ContactByEmail { get; set; }
        public int? ContactByPhone { get; set; }
        public int? ContactByPost { get; set; }

        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string CompanyName { get; set; }
        public string DisplayName { get; set; }
        public string InformalSalutation { get; set; }
        public string FormalSalutation { get; set; }
        public string InformalAddressee { get; set; }
        public string FormalAddressee { get; set; }

        public DateTime BirthDate { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime BackgroundVerifiedDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public DateTime MembersSince { get; set; }
        public DateTime RenewalDate { get; set; }

        static DateTime today = DateTime.Now;
        static DateTime initialTime = new DateTime(1, 1, 1);
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public Constituent(int? organizationId, int? statusId, int? volunteerProfileId, int? communicationPermission, int? backgoundStatus, int? preferredMOC, int? profileTypeId, int? imageId, int? isAffiliate, int? smallImageId, int? legID, int? isDeleted, int? gender, int? maritalStatus, int? educationLevel, int? employmentStatus, int? jobTitleId, int? contactTypeId, int? dataOrgId, int? legacyId, int? sourceId, int? prefixId, int? parentConstituentId, int? contactByEmail, int? contactByPhone, int? contactByPost, string prefix, string firstName, string middleName, string lastName, string suffix, string companyName, string displayName, string informalSalutation, string formalSalutation, string informalAddressee, string formalAddressee, DateTime birthDate, DateTime backgroundVerifiedDate, DateTime membersSince, DateTime renewalDate)
        {
            ConstituentId = generateId();
            OrganizationId = organizationId;
            StatusId = statusId;
            VolunteerProfileId = volunteerProfileId;
            CommunicationPermission = communicationPermission;
            BackgoundStatus = backgoundStatus;
            PreferredMOC = preferredMOC;
            ProfileTypeId = profileTypeId;
            ImageId = imageId;
            IsAffiliate = isAffiliate;
            SmallImageId = smallImageId;
            LegID = legID;
            IsDeleted = isDeleted;
            Gender = gender;
            MaritalStatus = maritalStatus;
            EducationLevel = educationLevel;
            EmploymentStatus = employmentStatus;
            JobTitleId = jobTitleId;
            ContactTypeId = contactTypeId;
            DataOrgId = dataOrgId;
            LegacyId = legacyId;
            SourceId = sourceId;
            PrefixId = prefixId;
            ParentConstituentId = parentConstituentId;
            ContactByEmail = contactByEmail;
            ContactByPhone = contactByPhone;
            ContactByPost = contactByPost;
            Prefix = prefix;
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            Suffix = suffix;
            CompanyName = companyName;
            DisplayName = displayName;
            InformalSalutation = informalSalutation;
            FormalSalutation = formalSalutation;
            InformalAddressee = informalAddressee;
            FormalAddressee = formalAddressee;
            BirthDate = birthDate;
            DateCreated = DateTime.UtcNow;
            BackgroundVerifiedDate = backgroundVerifiedDate;
            LastUpdated = DateTime.UtcNow;
            MembersSince = membersSince;
            RenewalDate = renewalDate;
        }

        public Constituent(int? constituentId, int? organizationId, int? statusId, int? volunteerProfileId, int? communicationPermission, int? backgoundStatus, int? preferredMOC, int? profileTypeId, int? imageId, int? isAffiliate, int? smallImageId, int? legID, int? isDeleted, int? gender, int? maritalStatus, int? educationLevel, int? employmentStatus, int? jobTitleId, int? contactTypeId, int? dataOrgId, int? legacyId, int? sourceId, int? prefixId, int? parentConstituentId, int? contactByEmail, int? contactByPhone, int? contactByPost, string prefix, string firstName, string middleName, string lastName, string suffix, string companyName, string displayName, string informalSalutation, string formalSalutation, string informalAddressee, string formalAddressee, DateTime birthDate, DateTime backgroundVerifiedDate, DateTime membersSince, DateTime renewalDate)
        {
            ConstituentId = constituentId;
            OrganizationId = organizationId;
            StatusId = statusId;
            VolunteerProfileId = volunteerProfileId;
            CommunicationPermission = communicationPermission;
            BackgoundStatus = backgoundStatus;
            PreferredMOC = preferredMOC;
            ProfileTypeId = profileTypeId;
            ImageId = imageId;
            IsAffiliate = isAffiliate;
            SmallImageId = smallImageId;
            LegID = legID;
            IsDeleted = isDeleted;
            Gender = gender;
            MaritalStatus = maritalStatus;
            EducationLevel = educationLevel;
            EmploymentStatus = employmentStatus;
            JobTitleId = jobTitleId;
            ContactTypeId = contactTypeId;
            DataOrgId = dataOrgId;
            LegacyId = legacyId;
            SourceId = sourceId;
            PrefixId = prefixId;
            ParentConstituentId = parentConstituentId;
            ContactByEmail = contactByEmail;
            ContactByPhone = contactByPhone;
            ContactByPost = contactByPost;
            Prefix = prefix;
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            Suffix = suffix;
            CompanyName = companyName;
            DisplayName = displayName;
            InformalSalutation = informalSalutation;
            FormalSalutation = formalSalutation;
            InformalAddressee = informalAddressee;
            FormalAddressee = formalAddressee;
            BirthDate = birthDate;
            DateCreated = DateTime.UtcNow;
            BackgroundVerifiedDate = backgroundVerifiedDate;
            LastUpdated = DateTime.UtcNow;
            MembersSince = membersSince;
            RenewalDate = renewalDate;
        }

        public Constituent()
        {
        }

        public static int validateLimitInt(int num)
        {
            if(num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }

        public int generateId()
        {
            return contadorId;
        }
        public Result isValid()
        {
            int contador = 0;
            Constituent data = new Constituent(ConstituentId,OrganizationId,StatusId,VolunteerProfileId,CommunicationPermission,
                BackgoundStatus,PreferredMOC,ProfileTypeId,ImageId,IsAffiliate,SmallImageId,LegID,IsDeleted,Gender,MaritalStatus,
                EducationLevel,EmploymentStatus,JobTitleId,ContactTypeId,DataOrgId,LegacyId,SourceId,PrefixId,ParentConstituentId,
                ContactByEmail,ContactByPhone,ContactByPost,Prefix,FirstName,MiddleName,LastName,Suffix,CompanyName,DisplayName,InformalSalutation,
                FormalSalutation,InformalAddressee,FormalAddressee,BirthDate,BackgroundVerifiedDate,
                MembersSince,RenewalDate);
            if (validateLimitInt((int)ConstituentId) == 0 ||
                validateLimitInt((int)OrganizationId) == 0 ||
                validateLimitInt((int)StatusId) == 0 ||
                validateLimitInt((int)VolunteerProfileId) == 0 ||
                validateLimitInt((int)CommunicationPermission) == 0 ||
                validateLimitInt((int)BackgoundStatus) == 0 ||
                validateLimitInt((int)PreferredMOC) == 0 ||
                validateLimitInt((int)ProfileTypeId) == 0 ||
                validateLimitInt((int)ImageId) == 0 ||
                validateLimitInt((int)IsAffiliate) == 0 ||
                validateLimitInt((int)SmallImageId) == 0 ||
                validateLimitInt((int)LegID) == 0 ||
                validateLimitInt((int)IsDeleted) == 0 ||
                validateLimitInt((int)Gender) == 0 ||
                validateLimitInt((int)MaritalStatus) == 0 ||
                validateLimitInt((int)EducationLevel) == 0 ||
                validateLimitInt((int)EmploymentStatus) == 0 ||
                validateLimitInt((int)JobTitleId) == 0 ||
                validateLimitInt((int)ContactTypeId) == 0 ||
                validateLimitInt((int)DataOrgId) == 0 ||
                validateLimitInt((int)LegacyId) == 0 ||
                validateLimitInt((int)SourceId) == 0 ||
                validateLimitInt((int)PrefixId) == 0 ||
                validateLimitInt((int)ParentConstituentId) == 0 ||
                validateLimitInt((int)ContactByEmail) == 0 ||
                validateLimitInt((int)ContactByPhone) == 0 ||
                validateLimitInt((int)ContactByPost) == 0 
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }

            if (verifyDate(BackgroundVerifiedDate) == false ||
                verifyDate(MembersSince) == false ||
                verifyDate(RenewalDate) == false 
                )
            {
                contador++;
                listError.Add(new Error(contador, "Las fechas no pueden ser menores de 3 anios."));
            }

            if (BirthDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta CampaignId"));
            }
            
            if (OrganizationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta OrganizationId"));
            }
           
            if (StatusId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta StatusId"));
            }
            
            if (VolunteerProfileId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta VolunteerProfileId"));
            }
            if (CommunicationPermission == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta CommunicationPermission"));
            }
            if (BackgoundStatus == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta BackgoundStatus"));
            }
            
            if (PreferredMOC == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta PreferredMOC"));
            }
            
            if (ProfileTypeId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ProfileTypeId"));
            }
            
            if (ImageId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ImageId"));
            }
            
            if (IsAffiliate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsAffiliate"));
            }
            
            if (SmallImageId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta SmallImageId"));
            }
           
            if (LegID == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LegID"));
            }
            
            if (Gender == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta Gender"));
            }
            
            if (MaritalStatus == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta MaritalStatus"));
            }
            
            if (EducationLevel == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta EducationLevel"));
            }
            
            if (EmploymentStatus == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta EmploymentStatus"));
            }
           
            if (JobTitleId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta JobTitleId"));
            }
            
            if (ContactTypeId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContactTypeId"));
            }
            
            if (DataOrgId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DataOrgId"));
            }
            
            if (LegacyId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LegacyId"));
            }
            
            if (SourceId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta SourceId"));
            }
            
            if (PrefixId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta PrefixId"));
            }
            
            if (ParentConstituentId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ParentConstituentId"));
            }
           
            if (ContactByEmail == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContactByEmail"));
            }
            
            if (ContactByPhone == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContactByPhone"));
            }
            
            if (ContactByPost == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContactByPost"));
            }
            
            if (string.IsNullOrWhiteSpace(Prefix))
            {
                contador++;
                listError.Add(new Error(contador, "Falta Prefix"));
            }
            if (string.IsNullOrWhiteSpace(FirstName))
            {
                contador++;
                listError.Add(new Error(contador, "Falta Prefix"));
            }
            else
            {
                if (FirstName.Length <= 3)
                {
                    contador++;
                    listError.Add(new Error(contador, "El nombre no puede ser menor a 3 letras"));
                }
            }
            if (string.IsNullOrWhiteSpace(FirstName))
            {
                contador++;
                listError.Add(new Error(contador, "Falta Prefix"));
            }
            else
            {
                if (FirstName.Length <= 3)
                {
                    contador++;
                    listError.Add(new Error(contador, "El nombre no puede ser menor a 3 letras"));
                }
            }

            if (string.IsNullOrWhiteSpace(MiddleName))
            {
                contador++;
                listError.Add(new Error(contador, "Falta Description"));
            }
            else
            {
                if (MiddleName.Length <= 3)
                {
                    contador++;
                    listError.Add(new Error(contador, "El nombre no puede ser menor a 3 letras"));
                }
            }
            if (string.IsNullOrWhiteSpace(LastName))
            {
                contador++;
                listError.Add(new Error(contador, "Falta LastName"));
            }
            else
            {
                if (LastName.Length <= 3)
                {
                    contador++;
                    listError.Add(new Error(contador, "El nombre no puede ser menor a 3 letras"));
                }
            }
            if (string.IsNullOrWhiteSpace(Suffix))
            {
                contador++;
                listError.Add(new Error(contador, "Falta Suffix"));
            }
            if (string.IsNullOrWhiteSpace(CompanyName))
            {
                contador++;
                listError.Add(new Error(contador, "Falta CompanyName"));
            }
            if (string.IsNullOrWhiteSpace(DisplayName))
            {
                contador++;
                listError.Add(new Error(contador, "Falta DisplayName"));
            }
            if (string.IsNullOrWhiteSpace(InformalSalutation))
            {
                contador++;
                listError.Add(new Error(contador, "Falta InformalSalutation"));
            }
            if (string.IsNullOrWhiteSpace(FormalSalutation))
            {
                contador++;
                listError.Add(new Error(contador, "Falta FormalSalutation"));
            }
            if (string.IsNullOrWhiteSpace(InformalAddressee))
            {
                contador++;
                listError.Add(new Error(contador, "Falta InformalAddressee"));
            }
            if (string.IsNullOrWhiteSpace(FormalAddressee))
            {
                contador++;
                listError.Add(new Error(contador, "Falta FormalAddressee"));
            }
            if (BirthDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta BirthDate"));
            }
            if (DateCreated == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DateCreated"));
            }
            if (BackgroundVerifiedDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta BackgroundVerifiedDate"));
            }
            if (LastUpdated == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LastUpdated"));
            }
            if (MembersSince == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta MembersSince"));
            }
            if (RenewalDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta RenewalDate"));
            }

            //last verification
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }
        public override string ToString()
        {
            string info = "ConstituentId: " + ConstituentId + "\n" +
                            "OrganizationId: " + OrganizationId + "\n" +
                            "StatusId: " + StatusId + "\n" +
                            "VolunteerProfileId: " + VolunteerProfileId + "\n" +
                            "CommunicationPermission: " + CommunicationPermission + "\n" +
                            "BackgoundStatus: " + BackgoundStatus + "\n" +
                            "PreferredMOC: " + PreferredMOC + "\n" +
                            "ProfileTypeId: " + ProfileTypeId + "\n" +
                            "ImageId: " + ImageId + "\n" +
                            "IsAffiliate: " + IsAffiliate + "\n" +
                            "SmallImageId: " + SmallImageId + "\n" +
                            "LegID: " + LegID + "\n" +
                            "IsDeleted: " + IsDeleted + "\n" +
                            "Gender: " + Gender + "\n" +
                            "MaritalStatus: " + MaritalStatus + "\n" +
                            "EducationLevel: " + EducationLevel + "\n" +
                            "EmploymentStatus: " + EmploymentStatus + "\n" +
                            "JobTitleId: " + JobTitleId + "\n" +
                            "ContactTypeId: " + ContactTypeId + "\n" +
                            "DataOrgId: " + DataOrgId + "\n" +
                            "LegacyId: " + LegacyId + "\n" +
                            "SourceId: " + SourceId + "\n" +
                            "PrefixId: " + PrefixId + "\n" +
                            "ParentConstituentId: " + ParentConstituentId + "\n" +
                            "ImContactByEmailageId: " + ContactByEmail + "\n" +
                            "ContactByPhone: " + ContactByPhone + "\n" +
                            "ContactByPost: " + ContactByPost + "\n" +
                            "Prefix: " + Prefix + "\n" +
                            "FirstName: " + FirstName + "\n" +
                            "MiddleName: " + MiddleName + "\n" +
                            "LastName: " + LastName + "\n" +
                            "Suffix: " + Suffix + "\n" +
                            "CompanyName: " + CompanyName + "\n" +
                            "DisplayName: " + DisplayName + "\n" +
                            "InformalSalutation: " + InformalSalutation + "\n" +
                            "FormalSalutation: " + FormalSalutation + "\n" +
                            "InformalAddressee: " + InformalAddressee + "\n" +
                            "FormalAddressee: " + FormalAddressee + "\n" +
                            "BirthDate: " + BirthDate + "\n" +
                            "DateCreated: " + DateCreated + "\n" +
                            "BackgroundVerifiedDate: " + BackgroundVerifiedDate + "\n" +
                            "LastUpdated: " + LastUpdated + "\n" +
                            "MembersSince: " + MembersSince + "\n" +
                            "RenewalDate: " + RenewalDate + "\n";

            return info;
        }

        public static Boolean verifyDate(DateTime date)
        {
            TimeSpan result = today - date;
            int years = (initialTime + result).Year - 1;
            if (years < 3)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}