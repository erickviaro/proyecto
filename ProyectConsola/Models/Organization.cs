﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class Organization
    {
        static Configuracion config = new Configuracion(2, 200);
        public int? OrganizationId { get; set; }
        public int? IsDeleted { get; set; }
        public int? IsPurged { get; set; }
        public string OrganizationGuid { get; set; }
        public string OrganizationName { get; set; }
        public string TimeZone { get; set; }
        public string WidgetLayout { get; set; }
        public string Preferences { get; set; }
        public string FiscalYear { get; set; }
        public DateTime SyncWithBigQuery { get; set; }

        static DateTime today = DateTime.Now;
        static DateTime initialTime = new DateTime(1, 1, 1);
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public Organization(int? organizationId, int? isDeleted, int? isPurged, string organizationGuid, 
                            string organizationName, string timeZone, string widgetLayout, 
                            string preferences, string fiscalYear, DateTime syncWithBigQuery)
        {
            OrganizationId = organizationId;
            IsDeleted = isDeleted;
            IsPurged = isPurged;
            OrganizationGuid = organizationGuid;
            OrganizationName = organizationName;
            TimeZone = timeZone;
            WidgetLayout = widgetLayout;
            Preferences = preferences;
            FiscalYear = fiscalYear;
            SyncWithBigQuery = syncWithBigQuery;
        }

        public Organization(int? isDeleted, int? isPurged, string organizationGuid,
                    string organizationName, string timeZone, string widgetLayout,
                    string preferences, string fiscalYear, DateTime syncWithBigQuery)
        {
            OrganizationId = generateId();
            IsDeleted = isDeleted;
            IsPurged = isPurged;
            OrganizationGuid = organizationGuid;
            OrganizationName = organizationName;
            TimeZone = timeZone;
            WidgetLayout = widgetLayout;
            Preferences = preferences;
            FiscalYear = fiscalYear;
            SyncWithBigQuery = syncWithBigQuery;
        }

        public Organization()
        {
        }

        public Result isValid()
        {
            int contador = 0;
            Organization data = new Organization(OrganizationId, IsDeleted, IsPurged, OrganizationGuid, OrganizationName,
                TimeZone, WidgetLayout, Preferences, FiscalYear, SyncWithBigQuery);

            if (validateLimitInt((int)OrganizationId) == 0 ||
                validateLimitInt((int)IsDeleted) == 0 ||
                validateLimitInt((int)IsPurged) == 0 
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }

            if (verifyDate(SyncWithBigQuery) == false)
            {
                contador++;
                listError.Add(new Error(contador, "Las fechas no pueden ser menores de 3 anios."));
            }

            if (OrganizationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta OrganizationId"));
            }
            if (IsDeleted == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsDeleted"));
            }
            if (IsPurged == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsPurged"));
            }
            if (SyncWithBigQuery == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta SyncWithBigQuery"));
            }
            if (string.IsNullOrWhiteSpace(OrganizationGuid))
            {
                contador++;
                listError.Add(new Error(contador, "Falta OrganizationGuid"));
            }
            if (string.IsNullOrWhiteSpace(OrganizationName))
            {
                contador++;
                listError.Add(new Error(contador, "Falta OrganizationName"));
            }
            if (string.IsNullOrWhiteSpace(TimeZone))
            {
                contador++;
                listError.Add(new Error(contador, "Falta TimeZone"));
            }
            if (string.IsNullOrWhiteSpace(WidgetLayout))
            {
                contador++;
                listError.Add(new Error(contador, "Falta WidgetLayout"));
            }
            if (string.IsNullOrWhiteSpace(Preferences))
            {
                contador++;
                listError.Add(new Error(contador, "Falta Preferences"));
            }
            if (string.IsNullOrWhiteSpace(FiscalYear))
            {
                contador++;
                listError.Add(new Error(contador, "Falta FiscalYear"));
            }

            //last validation
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }

        public override string ToString()
        {
            string info = "OrganizationId: " + OrganizationId + "\n" +
                            "IsDeleted: " + IsDeleted + "\n" +
                            "IsPurged: " + IsPurged + "\n" +
                            "OrganizationGuid: " + OrganizationGuid + "\n" +
                            "OrganizationName: " + OrganizationName + "\n" +
                            "TimeZone: " + TimeZone + "\n" +
                            "WidgetLayout: " + WidgetLayout + "\n" +
                            "Preferences: " + Preferences + "\n" +
                            "FiscalYear: " + FiscalYear + "\n" +
                            "SyncWithBigQuery: " + SyncWithBigQuery + "\n";
            return info;
        }

        public int generateId()
        {
            return contadorId;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }

        public static float convertFloat(float value, int digits)
        {
            double result = Math.Round(value, digits);
            return (float)result;
        }

        public static Boolean verifyDate(DateTime date)
        {
            TimeSpan result = today - date;
            int years = (initialTime + result).Year - 1;
            return !(years < 3);
        }
    }
}