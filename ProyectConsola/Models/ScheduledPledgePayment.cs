﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class ScheduledPledgePayment
    {
        static Configuracion config = new Configuracion(2, 200);
        public int? ScheduledPledgePaymentId { get; set; }
        public int? PledgeId { get; set; }
        public float? ExpectedAmount { get; set; }
        public float? ActualAmount { get; set; }
        public int? ExpectedPaymentMethod { get; set; }
        public int? ContributionId { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? LegacyId { get; set; }
        public int? DataOrgID { get; set; }
        public string ScheduledPledgePaymentGuid { get; set; } 
        public DateTime ExpectedDate { get; set; }
        public DateTime ActualDate { get; set; }

        static DateTime today = DateTime.Now;
        static DateTime initialTime = new DateTime(1, 1, 1);
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public ScheduledPledgePayment(int? scheduledPledgePaymentId, int? pledgeId, float? expectedAmount, float? actualAmount, int? expectedPaymentMethod, int? contributionId, int? paymentTypeId, int? legacyId, int? dataOrgID, string scheduledPledgePaymentGuid, DateTime expectedDate)
        {
            ScheduledPledgePaymentId = scheduledPledgePaymentId;
            PledgeId = pledgeId;
            ExpectedAmount = expectedAmount;
            ActualAmount = actualAmount;
            ExpectedPaymentMethod = expectedPaymentMethod;
            ContributionId = contributionId;
            PaymentTypeId = paymentTypeId;
            LegacyId = legacyId;
            DataOrgID = dataOrgID;
            ScheduledPledgePaymentGuid = scheduledPledgePaymentGuid;
            ExpectedDate = expectedDate;
            ActualDate = DateTime.UtcNow;
        }

        public ScheduledPledgePayment(int? pledgeId, float? expectedAmount, float? actualAmount, int? expectedPaymentMethod, int? contributionId, int? paymentTypeId, int? legacyId, int? dataOrgID, string scheduledPledgePaymentGuid, DateTime expectedDate)
        {
            ScheduledPledgePaymentId = generateId();
            PledgeId = pledgeId;
            ExpectedAmount = expectedAmount;
            ActualAmount = actualAmount;
            ExpectedPaymentMethod = expectedPaymentMethod;
            ContributionId = contributionId;
            PaymentTypeId = paymentTypeId;
            LegacyId = legacyId;
            DataOrgID = dataOrgID;
            ScheduledPledgePaymentGuid = scheduledPledgePaymentGuid;
            ExpectedDate = expectedDate;
            ActualDate = DateTime.UtcNow;
        }

        public ScheduledPledgePayment()
        {
        }

        public Result isValid()
        {
            int contador = 0;
            ScheduledPledgePayment data = new ScheduledPledgePayment(ScheduledPledgePaymentId, PledgeId, ExpectedAmount, 
                ActualAmount, ExpectedPaymentMethod, ContributionId, PaymentTypeId, LegacyId, DataOrgID, ScheduledPledgePaymentGuid,ExpectedDate);
            if (validateLimitInt((int)ScheduledPledgePaymentId) == 0 ||
                validateLimitInt((int)PledgeId) == 0 ||
                validateLimitInt((int)ExpectedPaymentMethod) == 0 ||
                validateLimitInt((int)ContributionId) == 0 ||
                validateLimitInt((int)PaymentTypeId) == 0 ||
                validateLimitInt((int)LegacyId) == 0 ||
                validateLimitInt((int)DataOrgID) == 0 
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }

            if (verifyDate(ExpectedDate) == false)
            {
                contador++;
                listError.Add(new Error(contador, "Las fechas no pueden ser menores de 3 anios."));
            }

            if (ScheduledPledgePaymentId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ScheduledPledgePaymentId"));
            }
            if (PledgeId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta PledgeId"));
            }
            if (ExpectedAmount == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ExpectedAmount"));
            }
            if (ActualAmount == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ActualAmount"));
            }
            if (ExpectedPaymentMethod == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ExpectedPaymentMethod"));
            }
            if (ContributionId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionId"));
            }
            if (PaymentTypeId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta PaymentTypeId"));
            }
            if (LegacyId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LegacyId"));
            }
            if (DataOrgID == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DataOrgID"));
            }
            if (ExpectedDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ExpectedDate"));
            }
            if (string.IsNullOrWhiteSpace(ScheduledPledgePaymentGuid))
            {
                contador++;
                listError.Add(new Error(contador, "Falta ScheduledPledgePaymentGuid"));
            }

            //last validation
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }

        public int generateId()
        {
            return contadorId;
        }

        public override string ToString()
        {
            ExpectedAmount = convertFloat((float)ExpectedAmount, (int)config.numeroDecimales);
            ActualAmount = convertFloat((float)ActualAmount, (int)config.numeroDecimales);

            string info = "ScheduledPledgePaymentId: " + ScheduledPledgePaymentId + "\n" +
                            "PledgeId: " + PledgeId + "\n" +
                            "ExpectedAmount: " + ExpectedAmount + "\n" +
                            "ActualAmount: " + ActualAmount + "\n" +
                            "ExpectedPaymentMethod: " + ExpectedPaymentMethod + "\n" +
                            "ContributionId: " + ContributionId + "\n" +
                            "PaymentTypeId: " + PaymentTypeId + "\n" +
                            "LegacyId: " + LegacyId + "\n" +
                            "DataOrgID: " + DataOrgID + "\n" +
                            "ScheduledPledgePaymentGuid: " + ScheduledPledgePaymentGuid + "\n" +
                            "ExpectedDate: " + ExpectedDate + "\n"+
                            "ActualDate: " + ActualDate + "\n";
            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }

        public static float convertFloat(float value, int digits)
        {
            double result = Math.Round(value, digits);
            return (float)result;
        }

        public static Boolean verifyDate(DateTime date)
        {
            TimeSpan result = today - date;
            int years = (initialTime + result).Year - 1;
            return !(years < 3);
        }
    }
}