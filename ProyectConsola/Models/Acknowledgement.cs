﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class Acknowledgement
    {
        static Configuracion config = new Configuracion(2, 2000);
        public int? AcknowledgementId { get; set; }
        public int? OrganizationId { get; set; }
        public int? IsVisible { get; set; }
        public string Description { get; set; }
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public Acknowledgement(int organizationId, int isVisible,
                                string description)
        {
            AcknowledgementId = generateId();
            OrganizationId = organizationId;
            IsVisible = isVisible;
            Description = description;
        }

        public Acknowledgement(int? acknowledgementId, int? organizationId, int? isVisible, string description)
        {
            AcknowledgementId = acknowledgementId;
            OrganizationId = organizationId;
            IsVisible = isVisible;
            Description = description;
        }

        public Acknowledgement()
        {
        }

        public int generateId()
        {
            return contadorId;
        }

        public Result isValid()
        {
            int contador = 0;
            Acknowledgement data = new Acknowledgement(AcknowledgementId, OrganizationId, IsVisible, Description);
            if (validateLimitInt((int)AcknowledgementId) == 0 ||
                validateLimitInt((int)OrganizationId) == 0 ||
                validateLimitInt((int)IsVisible) == 0 
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }
            if (AcknowledgementId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AcknowledgementId"));
            }
            if (OrganizationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta OrganizationId"));
            }
            if (IsVisible == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsVisible"));
            }
            if (Description == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta Description"));
            }
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }
        public override string ToString()
        {
            string info = "AcknowledgementId: " + AcknowledgementId + "\n" +
                            "OrganizationId: " + OrganizationId + "\n" +
                            "IsVisible: " + IsVisible + "\n" +
                            "Description: " + Description + "\n";
            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }
    }
}