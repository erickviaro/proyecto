﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class SoftCredit
    {
        static Configuracion config = new Configuracion(2, 200);
        public int? SoftCreditId { get; set; }
        public int? ConstituentId { get; set; }
        public int? ContributionId { get; set; }
        public float? CreditAmount { get; set; }
        public int? AcknowledgementId { get; set; }
        public int? AcknowledgementSTatus { get; set; }
        public int? ExcludeFromDonationDonorOverallGiving { get; set; }
        public int? IsDeleted { get; set; }
        public int? MailingListId { get; set; }
        public string ScheduledPledgePaymentGuid { get; set; }
        public DateTime AcknowledgementDate { get; set; }
        public DateTime InsertedDateTime { get; set; }
        public DateTime LastUpdatedDateTime { get; set; }

        static DateTime today = DateTime.Now;
        static DateTime initialTime = new DateTime(1, 1, 1);
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public SoftCredit(int? softCreditId, int? constituentId, int? contributionId, float? creditAmount, int? acknowledgementId, int? acknowledgementSTatus, int? excludeFromDonationDonorOverallGiving, int? isDeleted, int? mailingListId, string scheduledPledgePaymentGuid, DateTime acknowledgementDate)
        {
            SoftCreditId = softCreditId;
            ConstituentId = constituentId;
            ContributionId = contributionId;
            CreditAmount = creditAmount;
            AcknowledgementId = acknowledgementId;
            AcknowledgementSTatus = acknowledgementSTatus;
            ExcludeFromDonationDonorOverallGiving = excludeFromDonationDonorOverallGiving;
            IsDeleted = isDeleted;
            MailingListId = mailingListId;
            ScheduledPledgePaymentGuid = scheduledPledgePaymentGuid;
            AcknowledgementDate = acknowledgementDate;
            InsertedDateTime = DateTime.UtcNow;
            LastUpdatedDateTime = DateTime.UtcNow;
        }

        public SoftCredit(int? constituentId, int? contributionId, float? creditAmount, int? acknowledgementId, int? acknowledgementSTatus, int? excludeFromDonationDonorOverallGiving, int? isDeleted, int? mailingListId, string scheduledPledgePaymentGuid, DateTime acknowledgementDate)
        {
            SoftCreditId = generateId();
            ConstituentId = constituentId;
            ContributionId = contributionId;
            CreditAmount = creditAmount;
            AcknowledgementId = acknowledgementId;
            AcknowledgementSTatus = acknowledgementSTatus;
            ExcludeFromDonationDonorOverallGiving = excludeFromDonationDonorOverallGiving;
            IsDeleted = isDeleted;
            MailingListId = mailingListId;
            ScheduledPledgePaymentGuid = scheduledPledgePaymentGuid;
            AcknowledgementDate = acknowledgementDate;
            InsertedDateTime = DateTime.UtcNow;
            LastUpdatedDateTime = DateTime.UtcNow;
        }

        public SoftCredit()
        {
        }

        public Result isValid()
        {
            int contador = 0;
            SoftCredit data = new SoftCredit(SoftCreditId, ConstituentId, ContributionId,
                CreditAmount, AcknowledgementId, AcknowledgementSTatus, ExcludeFromDonationDonorOverallGiving,
                IsDeleted, MailingListId, ScheduledPledgePaymentGuid, AcknowledgementDate);
            if (validateLimitInt((int)SoftCreditId) == 0 ||
                validateLimitInt((int)ConstituentId) == 0 ||
                validateLimitInt((int)ContributionId) == 0 ||
                validateLimitInt((int)AcknowledgementId) == 0 ||
                validateLimitInt((int)AcknowledgementSTatus) == 0 ||
                validateLimitInt((int)ExcludeFromDonationDonorOverallGiving) == 0 ||
                validateLimitInt((int)IsDeleted) == 0 ||
                validateLimitInt((int)MailingListId) == 0

                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }

            if (SoftCreditId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta SoftCreditId"));
            }
            if (ConstituentId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ConstituentId"));
            }
            if (ContributionId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionId"));
            }
            if (CreditAmount == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta CreditAmount"));
            }
            if (AcknowledgementId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AcknowledgementId"));
            }
            if (AcknowledgementSTatus == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AcknowledgementSTatus"));
            }
            if (ExcludeFromDonationDonorOverallGiving == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ExcludeFromDonationDonorOverallGiving"));
            }
            if (IsDeleted == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsDeleted"));
            }
            if (MailingListId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta MailingListId"));
            }
            if (string.IsNullOrWhiteSpace(ScheduledPledgePaymentGuid))
            {
                contador++;
                listError.Add(new Error(contador, "Falta ScheduledPledgePaymentGuid"));
            }
            if (AcknowledgementDate == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AcknowledgementDate"));
            }
            if (InsertedDateTime == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta InsertedDateTime"));
            }
            if (LastUpdatedDateTime == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LastUpdatedDateTime"));
            }

            if (verifyDate(AcknowledgementDate) == false)
            {
                contador++;
                listError.Add(new Error(contador, "Las fechas no pueden ser menores de 3 anios."));
            }

            //last validation
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }

        public int generateId()
        {
            return contadorId;
        }

        public override string ToString()
        {
            CreditAmount = convertFloat((float)CreditAmount, (int)config.numeroDecimales);

            string info = "SoftCreditId: " + SoftCreditId + "\n" +
                            "ConstituentId: " + ConstituentId + "\n" +
                            "ContributionId: " + ContributionId + "\n" +
                            "CreditAmount: " + CreditAmount + "\n" +
                            "AcknowledgementId: " + AcknowledgementId + "\n" +
                            "AcknowledgementSTatus: " + AcknowledgementSTatus + "\n" +
                            "ExcludeFromDonationDonorOverallGiving: " + ExcludeFromDonationDonorOverallGiving + "\n" +
                            "IsDeleted: " + IsDeleted + "\n" +
                            "MailingListId: " + MailingListId + "\n" +
                            "ScheduledPledgePaymentGuid: " + ScheduledPledgePaymentGuid + "\n" +
                            "AcknowledgementDate: " + AcknowledgementDate + "\n" +
                            "InsertedDateTime: " + InsertedDateTime + "\n" +
                            "LastUpdatedDateTime: " + LastUpdatedDateTime + "\n";
            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }

        public static float convertFloat(float value, int digits)
        {
            double result = Math.Round(value, digits);
            return (float)result;
        }

        public static Boolean verifyDate(DateTime date)
        {
            TimeSpan result = today - date;
            int years = (initialTime + result).Year - 1;
            return !(years < 3);
        }
    }
}