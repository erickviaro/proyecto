﻿using ProyectConsola.Models;
using ProyectConsola.Repository;
using ProyectConsola.Services;
using Proyecto.Filters;
using Proyecto.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class ShippingAddress
    {
        static Configuracion config = new Configuracion(2, 200);
        public int? ShippingAddressId { get; set; }
        public int? IsDeleted { get; set; }
        public int? ContributionId { get; set; }
        public int? AddresId { get; set; }
        public string ShippingName { get; set; }
        public string ReceipientPhoneNumber { get; set; }

        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public ShippingAddress(int? isDeleted, int? contributionId, 
                                int? addresId, string shippingName, string receipientPhoneNumber)
        {
            ShippingAddressId = generateId();
            IsDeleted = isDeleted;
            ContributionId = contributionId;
            AddresId = addresId;
            ShippingName = shippingName;
            ReceipientPhoneNumber = receipientPhoneNumber;
        }

        public ShippingAddress(int? shippingAddressId, int? isDeleted, int? contributionId, int? addresId, string shippingName, string receipientPhoneNumber)
        {
            ShippingAddressId = shippingAddressId;
            IsDeleted = isDeleted;
            ContributionId = contributionId;
            AddresId = addresId;
            ShippingName = shippingName;
            ReceipientPhoneNumber = receipientPhoneNumber;
        }

        public int generateId()
        {
            return contadorId;
        }

        public ShippingAddress()
        {
        }

        public Result isValid()
        {
            int contador = 0;
            ShippingAddress data = new ShippingAddress(ShippingAddressId, IsDeleted, ContributionId, AddresId, ShippingName, ReceipientPhoneNumber);
            if (validateLimitInt((int)ShippingAddressId) == 0 ||
                validateLimitInt((int)IsDeleted) == 0 ||
                validateLimitInt((int)ContributionId) == 0 ||
                validateLimitInt((int)AddresId) == 0 
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }
            if (ShippingAddressId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ShippingAddressId"));
            }
            if (IsDeleted == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsDeleted"));
            }
            if (ContributionId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionId"));
            }
            if (AddresId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AddresId"));
            }
            if (string.IsNullOrWhiteSpace(ShippingName))
            {
                contador++;
                listError.Add(new Error(contador, "Falta ShippingName"));
            }
            if (string.IsNullOrWhiteSpace(ReceipientPhoneNumber))
            {
                contador++;
                listError.Add(new Error(contador, "Falta ReceipientPhoneNumber"));
            }

            //last validation
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }

        }

        public override string ToString()
        {
            string info = "ShippingAddressId: " + ShippingAddressId + "\n" +
                            "IsDeleted: " + IsDeleted + "\n" +
                            "ContributionId: " + ContributionId + "\n" +
                            "AddresId: " + AddresId + "\n" +
                            "ShippingName: " + ShippingName + "\n" +
                            "ReceipientPhoneNumber: " + ReceipientPhoneNumber + "\n";
            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }
    }
}