﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class Designation
    {
        static Configuracion config = new Configuracion(2, 2000);
        public int? DesignationId { get; set; }
        public int? OrganizationId { get; set; }
        public int? IsVisible { get; set; }
        public string Name { get; set; }
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public Designation(int? organizationId, int? isVisible, string name)
        {
            DesignationId = generateId();
            OrganizationId = organizationId;
            IsVisible = isVisible;
            Name = name;
        }

        public Designation(int? designationId, int? organizationId, int? isVisible, string name)
        {
            DesignationId = designationId;
            OrganizationId = organizationId;
            IsVisible = isVisible;
            Name = name;
        }

        public Designation()
        {
        }

        public int generateId()
        {
            return contadorId;
        }

        public Result isValid()
        {
            int contador = 0;
            Designation data = new Designation(DesignationId, OrganizationId, IsVisible, Name);
            if (validateLimitInt((int)DesignationId) == 0 ||
                validateLimitInt((int)OrganizationId) == 0 ||
                validateLimitInt((int)IsVisible) == 0 
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }

            if (DesignationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DesignationId"));
            }
           
            if (OrganizationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta OrganizationId"));
            }
            
            if (IsVisible == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsVisible"));
            }

            if (string.IsNullOrWhiteSpace(Name))
            {
                contador++;
                listError.Add(new Error(contador, "Falta LastName"));
            }
            else
            {
                if (Name.Length <= 3)
                {
                    contador++;
                    listError.Add(new Error(contador, "El nombre no puede ser menor a 3 letras"));
                }
            }

            //last verification
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }
        public override string ToString()
        {
            string info = "DesignationId: " + DesignationId + "\n" +
                            "OrganizationId: " + OrganizationId + "\n" +
                            "IsVisible: " + IsVisible + "\n" +
                            "Name: " + Name + "\n";

            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }
    }
}