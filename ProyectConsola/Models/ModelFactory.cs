﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class ModelFactory
    {
        public ShippingAddress Parse(ShippingAddress model)
        {
            try
            {
                var entry = new ShippingAddress();
                if (model.AddresId != null)
                {
                    entry.AddresId = model.AddresId;
                }
                if (model.IsDeleted != null)
                {
                    entry.IsDeleted = model.IsDeleted;
                }
                if (model.ContributionId != null)
                {
                    entry.ContributionId = model.ContributionId;
                }
                if (!string.IsNullOrWhiteSpace(model.ShippingName))
                {
                    entry.ShippingName = model.ShippingName;
                }
                if (!string.IsNullOrWhiteSpace(model.ReceipientPhoneNumber))
                {
                    entry.ReceipientPhoneNumber = model.ReceipientPhoneNumber;
                }

                return entry;
            }
            catch
            {
                return null;
            }
        }
    }
}