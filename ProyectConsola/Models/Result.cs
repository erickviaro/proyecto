﻿using Proyecto.Interfaces;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Filters
{
    public class Result
    {
        public bool resultado { get; set; }
        public List<Error> listaErrores { get; set; }
        public object generic { get; set; }

        public Result(bool resultado, List<Error> listaErrores, object generic)
        {
            this.resultado = resultado;
            this.listaErrores = listaErrores;
            this.generic = generic;
        }
    }
}