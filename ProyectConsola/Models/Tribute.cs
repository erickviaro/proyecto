﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class Tribute
    {
        static Configuracion config = new Configuracion(2, 2000);
        public int? TributeId { get; set; }
        public int? TributeType { get; set; }
        public int? LegacyID { get; set; }
        public int? DataOrgID { get; set; }
        public int? IsVisible { get; set; }
        public int? TributeOrder { get; set; }
        public int? OrganizationId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public Tribute(int? tributeId, int? tributeType, int? legacyID, int? dataOrgID, int? isVisible, 
                        int? tributeOrder, int? organizationId, string name, string title, string description)
        {
            TributeId = tributeId;
            TributeType = tributeType;
            LegacyID = legacyID;
            DataOrgID = dataOrgID;
            IsVisible = isVisible;
            TributeOrder = tributeOrder;
            OrganizationId = organizationId;
            Name = name;
            Title = title;
            Description = description;
        }

        public Tribute(int? tributeType, int? legacyID, int? dataOrgID, int? isVisible,
                int? tributeOrder, int? organizationId, string name, string title, string description)
        {
            TributeId = generateId();
            TributeType = tributeType;
            LegacyID = legacyID;
            DataOrgID = dataOrgID;
            IsVisible = isVisible;
            TributeOrder = tributeOrder;
            OrganizationId = organizationId;
            Name = name;
            Title = title;
            Description = description;
        }

        public Tribute()
        {
        }

        public int generateId()
        {
            return contadorId;
        }

        public Result isValid()
        {
            int contador = 0;
            Tribute data = new Tribute(TributeId, TributeType, LegacyID, DataOrgID, IsVisible, TributeOrder,
                OrganizationId, Name, Title, Description);

            if (validateLimitInt((int)TributeId) == 0 ||
                validateLimitInt((int)TributeType) == 0 ||
                validateLimitInt((int)LegacyID) == 0 ||
                validateLimitInt((int)DataOrgID) == 0 ||
                validateLimitInt((int)IsVisible) == 0 ||
                validateLimitInt((int)TributeOrder) == 0 ||
                validateLimitInt((int)OrganizationId) == 0 
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }
            if (string.IsNullOrWhiteSpace(Title))
            {
                contador++;
                listError.Add(new Error(contador, "Falta Name"));
            }
            if (string.IsNullOrWhiteSpace(Description))
            {
                contador++;
                listError.Add(new Error(contador, "Falta Name"));
            }

            if (string.IsNullOrWhiteSpace(Name))
            {
                contador++;
                listError.Add(new Error(contador, "Falta Name"));
            }
            else
            {
                if (Name.Length <= 3)
                {
                    contador++;
                    listError.Add(new Error(contador, "El nombre no puede ser menor a 3 letras"));
                }
            }

            //last validation
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }

        public override string ToString()
        {
            string info = "TributeId: " + TributeId + "\n" +
                            "TributeType: " + TributeType + "\n" +
                            "LegacyID: " + LegacyID + "\n" +
                            "DataOrgID: " + DataOrgID + "\n" +
                            "IsVisible: " + IsVisible + "\n" +
                            "TributeOrder: " + TributeOrder + "\n" +
                            "OrganizationId: " + OrganizationId + "\n" +
                            "Name: " + Name + "\n" +
                            "Title: " + Title + "\n" +
                            "Description: " + Description + "\n";


            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }
    }
}