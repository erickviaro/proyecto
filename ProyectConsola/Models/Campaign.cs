﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class Campaign
    {
        static Configuracion config = new Configuracion(2, 2000);
        public int? CampaignId { get; set; }
        public int? OrganizationId { get; set; }
        public int? DataOrgId { get; set; }
        public int? LegacyID { get; set; }
        public int? IsVisible { get; set; }
        public int? Order { get; set; }
        public int? Goal { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public Campaign(int? organizationId, int? dataOrgId, int? legacyID, int? isVisible, 
                        int? order, int? goal, string name, string description)
        {
            CampaignId = generateId();
            OrganizationId = organizationId;
            DataOrgId = dataOrgId;
            LegacyID = legacyID;
            IsVisible = isVisible;
            Order = order;
            Goal = goal;
            Name = name;
            Description = description;
        }

        public Campaign(int? campaignId, int? organizationId, int? dataOrgId, int? legacyID, int? isVisible, int? order, int? goal, string name, string description)
        {
            CampaignId = campaignId;
            OrganizationId = organizationId;
            DataOrgId = dataOrgId;
            LegacyID = legacyID;
            IsVisible = isVisible;
            Order = order;
            Goal = goal;
            Name = name;
            Description = description;
        }

        public Campaign()
        {
        }
        public int generateId()
        {
            return contadorId;
        }
        public Result isValid()
        {
            int contador = 0;
            Campaign data = new Campaign(CampaignId, OrganizationId, DataOrgId, LegacyID, IsVisible, Order, Goal, Name, Description);
            if (validateLimitInt((int)CampaignId) == 0 ||
            validateLimitInt((int)OrganizationId) == 0 ||
            validateLimitInt((int)DataOrgId) == 0 ||
            validateLimitInt((int)LegacyID) == 0 ||
            validateLimitInt((int)IsVisible) == 0 ||
            validateLimitInt((int)Order) == 0 ||
            validateLimitInt((int)Goal) == 0
            )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }
            if (CampaignId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta CampaignId"));
            }

            if (OrganizationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta OrganizationId"));
            }
           
            if (DataOrgId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DataOrgId"));
            }
            
            if (LegacyID == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LegacyID"));
            }
            
            if (IsVisible == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsVisible"));
            }
            
            if (Order == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta Order"));
            }
            
            if (Goal == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta Goal"));
            }
            
            if (string.IsNullOrWhiteSpace(Name))
            {
                contador++;
                listError.Add(new Error(contador, "Falta Name"));
            }
            else
            {
                if (Name.Length <= 3)
                {
                    contador++;
                    listError.Add(new Error(contador, "El nombre no puede ser menor a 3 letras"));
                }
            }
            if (string.IsNullOrWhiteSpace(Description))
            {
                contador++;
                listError.Add(new Error(contador, "Falta Description"));
            }

            //last verification
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }
        public override string ToString()
        {
            string info = "CampaignId: " + CampaignId + "\n" +
                            "OrganizationId: " + OrganizationId + "\n" +
                            "DataOrgId: " + DataOrgId + "\n" +
                            "LegacyID: " + LegacyID + "\n" +
                            "IsVisible: " + IsVisible + "\n" +
                            "Order: " + Order + "\n" +
                            "Goal: " + Goal + "\n" +
                            "Name: " + Name + "\n" +
                            "Description: " + Description + "\n";
            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }
    }
}