﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class Appeal
    {
        static Configuracion config = new Configuracion(2, 2000);
        public int? AppealId { get; set; }
        public int? OrganizationId { get; set; }
        public int? IsVisible { get; set; }
        public int? AppealOrder { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public Appeal(int? organizationId, int? isVisible, int? appealOrder, string name, 
                        string description)
        {
            AppealId = generateId();
            OrganizationId = organizationId;
            IsVisible = isVisible;
            AppealOrder = appealOrder;
            Name = name;
            Description = description;
        }

        public Appeal(int? appealId, int? organizationId, int? isVisible, int? appealOrder, string name, string description)
        {
            AppealId = appealId;
            OrganizationId = organizationId;
            IsVisible = isVisible;
            AppealOrder = appealOrder;
            Name = name;
            Description = description;
        }

        public Appeal()
        {
        }

        public int generateId()
        {
            return contadorId;
        }
        public Result isValid()
        {
            int contador = 0;
            Appeal data = new Appeal(AppealId, OrganizationId, IsVisible, AppealOrder, Name, Description);
            if (validateLimitInt((int)AppealId) == 0 ||
                validateLimitInt((int)OrganizationId) == 0 ||
                validateLimitInt((int)IsVisible) == 0 ||
                validateLimitInt((int)AppealOrder) == 0
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }
            if (AppealId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AppealId"));
            }
            if (OrganizationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta OrganizationId"));
            }
            if (IsVisible == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta IsVisible"));
            }
            if (AppealOrder == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AppealOrder"));
            }
            if (Name == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta Name"));
            }
            else
            {
                if(Name.Length <= 3)
                {
                    contador++;
                    listError.Add(new Error(contador, "El nombre no puede ser menor a 3 letras"));
                }
            }

            //last verification
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }
        public override string ToString()
        {
            string info = "AppealId: " + AppealId + "\n" +
                            "OrganizationId: " + OrganizationId + "\n" +
                            "IsVisible: " + IsVisible + "\n" +
                            "AppealOrder: " + AppealOrder + "\n" +
                            "Name: " + Name + "\n";

            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }
    }
}