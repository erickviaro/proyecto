﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Filters
{
    public class Error
    {
        int id;
        string descripcion;

        public Error(int id, string descripcion)
        {
            this.id = id;
            this.descripcion = descripcion;
        }

        public Error()
        {
        }

        public override string ToString()
        {
           return "id: " + id + "\n" + "Descripcion: " + descripcion;
        }
    }
}