﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class FundAllocation
    {
        static Configuracion config = new Configuracion(2, 200);
        public int? FundAllocationId { get; set; }
        public int? FundId { get; set; }
        public int? ContributionId { get; set; }
        public float? Amount { get; set; }
        public int? DataOrgId { get; set; }
        public int? LegacyId { get; set; }
        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public FundAllocation(int? fundAllocationId, int? fundId, int? contributionId, float? amount, 
                                int? dataOrgId, int? legacyId)
        {
            FundAllocationId = fundAllocationId;
            FundId = fundId;
            ContributionId = contributionId;
            Amount = amount;
            DataOrgId = dataOrgId;
            LegacyId = legacyId;
        }

        public FundAllocation(int? fundId, int? contributionId, float? amount, int? dataOrgId, int? legacyId)
        {
            FundAllocationId = generateId();
            FundId = fundId;
            ContributionId = contributionId;
            Amount = amount;
            DataOrgId = dataOrgId;
            LegacyId = legacyId;
        }

        public FundAllocation()
        {
        }

        public Result isValid()
        {
            int contador = 0;
            FundAllocation data = new FundAllocation(FundAllocationId, FundId, ContributionId, Amount, DataOrgId, LegacyId);
            if (validateLimitInt((int)FundAllocationId) == 0 ||
                validateLimitInt((int)FundId) == 0 ||
                validateLimitInt((int)ContributionId) == 0 ||
                validateLimitInt((int)DataOrgId) == 0 ||
                validateLimitInt((int)LegacyId) == 0
                )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de" + config.limiteNumerico));
            }
            if (FundAllocationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta FundAllocationId"));
            }
            if (FundId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta FundId"));
            }
            if (ContributionId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta ContributionId"));
            }
            if (Amount == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta Amount"));
            }
            if (DataOrgId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta DataOrgId"));
            }
            if (LegacyId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta LegacyId"));
            }

            //last validation
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }

        public int generateId()
        {
            return contadorId;
        }

        public override string ToString()
        {
            Amount = convertFloat((float)Amount, (int)config.numeroDecimales);
            string info = "FundAllocationId: " + FundAllocationId + "\n" +
                            "FundId: " + FundId + "\n" +
                            "ContributionId: " + ContributionId + "\n" +
                            "Amount: " + Amount + "\n" +
                            "DataOrgId: " + DataOrgId + "\n" +
                            "LegacyId: " + LegacyId + "\n";
            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }

        public static float convertFloat(float value, int digits)
        {
            double result = Math.Round(value, digits);
            return (float)result;
        }

    }
}