﻿using ProyectConsola.Models;
using Proyecto.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class Solicitor
    {
        static Configuracion config = new Configuracion(2, 200);
        public int? SolicitorId { get; set; }
        public int? OrganizationId { get; set; }
        public int? IsVisible { get; set; }
        public string Name { get; set; }

        List<Error> listError = new List<Error>();
        static int contadorId = 1;

        public Solicitor(int? solicitorId, int? organizationId, int? isVisible, string name)
        {
            SolicitorId = solicitorId;
            OrganizationId = organizationId;
            IsVisible = isVisible;
            Name = name;
        }

        public Solicitor(int? organizationId, int? isVisible, string name)
        {
            SolicitorId = generateId();
            OrganizationId = organizationId;
            IsVisible = isVisible;
            Name = name;
        }

        public Solicitor()
        {
        }

        public Result isValid()
        {
            int contador = 0;
            Solicitor data = new Solicitor(SolicitorId, OrganizationId, IsVisible,Name);
            if (validateLimitInt((int)SolicitorId) == 0 ||
                 validateLimitInt((int)OrganizationId) == 0 ||
                 validateLimitInt((int)IsVisible) == 0 
                 )
            {
                contador++;
                listError.Add(new Error(contador, "Los campos numericos no puede ser menores a 1 ni mayores de " + config.limiteNumerico));
            }
            if (SolicitorId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AppealOrder"));
            }
            if (OrganizationId == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AppealOrder"));
            }
            if (IsVisible == null)
            {
                contador++;
                listError.Add(new Error(contador, "Falta AppealOrder"));
            }
            if (string.IsNullOrWhiteSpace(Name))
            {
                contador++;
                listError.Add(new Error(contador, "Falta Name"));
            }
            else
            {
                if (Name.Length <= 3)
                {
                    contador++;
                    listError.Add(new Error(contador, "El nombre no puede ser menor a 3 letras"));
                }
            }

            //last validation
            if (contador > 0)
            {
                Result result = new Result(false, listError, null);
                return result;
            }
            else
            {
                contadorId++;
                Result result = new Result(true, listError, data);
                return result;
            }
        }

        public int generateId()
        {
            return contadorId;
        }

        public override string ToString()
        {
            string info = "SolicitorId: " + SolicitorId + "\n" +
                            "OrganizationId: " + OrganizationId + "\n" +
                            "IsVisible: " + IsVisible + "\n" +
                            "Name: " + Name + "\n";
            return info;
        }

        public static int validateLimitInt(int num)
        {
            if (num < 1 || num >= config.limiteNumerico)
            {
                return 0;
            }
            else
            {
                return num;
            }
        }
    }
}