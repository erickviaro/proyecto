﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProyectConsola.Models
{
    class Configuracion
    {
        public int numeroDecimales { get; set; }
        public int limiteNumerico { get; set; }
        public Regex regex { get; set; }

        public Configuracion(int numeroDecimales, int limiteNumerico)
        {
            this.numeroDecimales = numeroDecimales;
            this.limiteNumerico = limiteNumerico;
            this.regex = new Regex(@"(@)(.+)$");
        }


    }
}
